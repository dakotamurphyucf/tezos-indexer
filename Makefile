# Open Source License
# Copyright (c) 2019-2020 Nomadic Labs <contact@nomadic-labs.com>
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.


SHELL=/bin/bash

all:
	@$(MAKE) tezos
	@$(MAKE) protos
	@rm src/bin_indexer/dune
	@$(MAKE) build

alpha:
	ALPHA=x make all

# makefile to make a local copy of tezos
include tezos.Makefile

# makefile to install opam packages
include opam.Makefile

# makefile to generate database schema
include db.Makefile

# makefile to test docker builds
include docker.Makefile

# makefile for git-related things
include git.Makefile


build:src/bin_indexer/dune
	dune build ./_build/default/src/bin_indexer/main_indexer.exe
	cp -f ./_build/default/src/bin_indexer/main_indexer.exe tezos-indexer

GENERATED_FILES = \
	src/bin_indexer/db_schema.ml \
	src/bin_indexer/version.ml \
	src/bin_indexer/snapshot_utils_6.ml \
	src/bin_indexer/snapshot_utils_7.ml \
	src/bin_indexer/snapshot_utils_6.mli \
	src/bin_indexer/snapshot_utils_7.mli \
	src/bin_indexer/indexer_helpers.ml \
	src/proto_001_PtCJ7pwo \
	src/proto_002_PsYLVpVv \
	src/proto_003_PsddFKi3 \
	src/proto_004_Pt24m4xi \
	src/proto_005_PsBabyM1 \
	src/proto_006_PsCARTHA \
	src/proto_007_PsDELPH1 \
	src/proto_008_PtEdo2Zk \
	src/proto_009_PsFLoren \
	src/proto_010_PtGRANAD \
	src/proto_011_PtHangz2 \
	src/proto_012_Psithaca \
	src/proto_alpha \
	src/bin_indexer/chain_db_transition_001_002.ml  \
	src/bin_indexer/chain_db_transition_001_002.mli \
	src/bin_indexer/chain_db_transition_002_003.ml  \
	src/bin_indexer/chain_db_transition_002_003.mli \
	src/bin_indexer/chain_db_transition_003_004.ml  \
	src/bin_indexer/chain_db_transition_003_004.mli \
	src/bin_indexer/chain_db_transition_004_005.ml  \
	src/bin_indexer/chain_db_transition_004_005.mli \
	src/bin_indexer/chain_db_transition_005_006.ml  \
	src/bin_indexer/chain_db_transition_005_006.mli \
	src/bin_indexer/mempool_utils_transition_011_012.ml  \
	src/bin_indexer/mempool_utils_transition_012_A.ml  \
	src/bin_indexer/chain_db_transition_006_007.ml  \
	src/bin_indexer/chain_db_transition_006_007.mli \
	src/bin_indexer/chain_db_transition_007_008.ml  \
	src/bin_indexer/chain_db_transition_007_008.mli \
	src/bin_indexer/chain_db_transition_008_009.ml  \
	src/bin_indexer/chain_db_transition_008_009.mli \
	src/bin_indexer/chain_db_transition_009_010.ml  \
	src/bin_indexer/chain_db_transition_009_010.mli \
	src/bin_indexer/chain_db_transition_010_011.ml  \
	src/bin_indexer/chain_db_transition_010_011.mli \
	src/bin_indexer/chain_db_transition_011_012.ml  \
	src/bin_indexer/chain_db_transition_011_012.mli \
	src/bin_indexer/chain_db_transition_012_A.ml  \
	src/bin_indexer/chain_db_transition_012_A.mli


protos: $(GENERATED_FILES)

src/bin_indexer/version.ml:Makefile $(shell ls -1 src/*/* | grep -v src/bin_indexer/version.ml) .git/*
	printf 'let c = "(" ^ String.lowercase_ascii @@ String.trim "commit: %s, %s" ^ ")"' "$$(git show|head -n 1|sed -e 's/commit //' -e 's/  */ /g')" "$$(TZ=UTC git log --date=iso-local|head -n 3|tail -n 1|sed -e 's/  */ /g') " > $@
	printf 'let t = "%s"' $$(git log --decorate=full |head -n 1|grep 'tag: refs/tags/'| sed 's|.*tag: refs/tags/||'|tr -d ')'|sed 's/,.*//') >> $@
	printf 'let t = if t <> "" then t else "(dev)"' >> $@
	printf 'let b = "(branch: %s)"' "$$(git branch |grep '*'|tr -d '*() ')" >> $@
	printf 'let sql = "%s" let sql_dev = %s ' \
		"$$(grep -e '-- version' < src/db-schema/versions.sql | sed -e "s/.*'\(.*\)'.*/\1/")" \
		"$$(grep -e '-- dev' < src/db-schema/versions.sql | grep -oE '(true|false)')" \
		>> $@
	echo 'let version () = Printf.printf "tezos-indexer %s %s %s %s\n%!" sql t b c; exit 0' >> $@


src/bin_indexer/db_schema.ml:Makefile src/db-schema/*
	>&2 ${MAKE} -s --no-print-directory db-schema-all-default > tmp.sql
	echo 'really_input_string stdin (in_channel_length stdin) |> Printf.printf "let s = %S let print () = print_endline s"' > tmp.ml
	ocaml tmp.ml < tmp.sql > $@
	echo >> $@
	>&2 ${MAKE} -s --no-print-directory db-schema-all-multicore > tmp.sql
	echo 'really_input_string stdin (in_channel_length stdin) |> Printf.printf "let m = %S let print_multicore () = print_endline m"' > tmp.ml
	ocaml tmp.ml < tmp.sql >> $@
	rm -f tmp.sql tmp.ml

ifdef ALPHA
MPP = mpp -so '' -sc '' -t ocaml -son "(**\#" -scn "\#**)" -sos-noloc '' -sos '' -scs '' -soc '' -scc '' -sec ''
else
MPP = mpp -so '' -sc '' -t ocaml -son "(**\#" -scn "\#**)" -sos-noloc '' -sos '' -scs '' -soc '' -scc '' -sec '' -set __NOALPHA__
endif


BIGMAPDIFF =  -set __BIG_MAP_DIFF__ -set __BIG_MAP__
STORAGEDIFF =  -set __STORAGE_DIFF__ -set __BIG_MAP__
MILLIGAS = -set __MILLIGAS__
META1 =  -set __META1__
META2 =  -set __META2__ -set __META_23__
META3 =  -set __META3__ -set __META_23__ -set __META_34__
META4 =  -set __META4__ -set __META_34__
META5 =  -set __META5__ -set __META_45__
META6 =  -set __META6__ -set __META_56__ -set __META_67__
META7 =  -set __META7__ -set __META_67__ -set __META_78__
META8 =  -set __META8__ -set __META_78__

MEMPOOL = -set __MEMPOOL__
FA2 = -set __FA2__

MPP_NO_OP = ${MPP} -set __NO_OP__

# block 1
PROTO0  = ${MPP_NO_OP} ${META1} -set PROTO=000_Ps9mPmXa -set PROTONEXT=000_Ps9mPmXa -set PROTONAME=000-Ps9mPmXa -set __PROTO0__ -set BS=BS0_mainnet
PROTO0C = ${MPP_NO_OP} ${META2} -set PROTO=000_PtYuensg -set PROTONEXT=006_PsCARTHA -set PROTONAME=000-PtYuensg -set __PROTO0__ -set BS=BS0_carthagenet
PROTO0D = ${MPP_NO_OP} ${META2} -set PROTO=000_PtYuensg -set PROTONEXT=007_PsDELPH1 -set PROTONAME=000_PtYuensg -set __PROTO0__ -set BS=BS0_delphinet
PROTO0E = ${MPP_NO_OP} ${META3} -set PROTO=000_PtYuensg -set PROTONEXT=000_Ps9mPmXa -set PROTONAME=000-PtYuensg -set __PROTO0__ -set BS=BS0_edonet
PROTO0F = ${MPP_NO_OP} ${META3} -set PROTO=000_PtYuensg -set PROTONEXT=000_Ps9mPmXa -set PROTONAME=000-PtYuensg -set __PROTO0__ -set BS=BS0_florencenet

# "normal" blocks
PROTO1 = ${MPP}               ${META1} -set PROTO=001_PtCJ7pwo -set PROTONEXT=001_PtCJ7pwo -set PROTONAME=001-PtCJ7pwo -set __PROTO1__ -set BS=BS1
PROTO2 = ${MPP} ${BIGMAPDIFF} ${META1} -set PROTO=002_PsYLVpVv -set PROTONEXT=002_PsYLVpVv -set PROTONAME=002-PsYLVpVv -set __PROTO2__ -set BS=BS2 -set PPBS=BS1 -set PBS=BST12
PROTO3 = ${MPP} ${BIGMAPDIFF} ${META1} -set PROTO=003_PsddFKi3 -set PROTONEXT=003_PsddFKi3 -set PROTONAME=003-PsddFKi3 -set __PROTO3__ -set BS=BS3 -set PPBS=BS2 -set PBS=BST23
PROTO4 = ${MPP} ${BIGMAPDIFF} ${META1} -set PROTO=004_Pt24m4xi -set PROTONEXT=004_Pt24m4xi -set PROTONAME=004-Pt24m4xi -set __PROTO4__ -set BS=BS4 -set PPBS=BS3 -set PBS=BST34
PROTO5 = ${MPP} ${BIGMAPDIFF} ${META2} -set PROTO=005_PsBabyM1 -set PROTONEXT=005_PsBabyM1 -set PROTONAME=005-PsBabyM1 -set __PROTO5__ -set BS=BS5 -set PPBS=BS4 -set PBS=BST45
PROTO6 = ${MPP} ${BIGMAPDIFF} ${META2} -set PROTO=006_PsCARTHA -set PROTONEXT=006_PsCARTHA -set PROTONAME=006-PsCARTHA -set __PROTO6__ -set BS=BS6 -set PPBS=BS5 -set PBS=BST56 ${MEMPOOL} ${FA2}
PROTO7 = ${MPP} ${BIGMAPDIFF} ${META2} -set PROTO=007_PsDELPH1 -set PROTONEXT=007_PsDELPH1 -set PROTONAME=007-PsDELPH1 -set __PROTO7__ -set BS=BS7 -set PPBS=BS6 -set PBS=BST67 ${MEMPOOL} ${MILLIGAS} ${FA2}
PROTO8 = ${MPP} ${STORAGEDIFF} ${META3} -set PROTO=008_PtEdo2Zk -set PROTONEXT=008_PtEdo2Zk -set PROTONAME=008-PtEdo2Zk -set __PROTO8__ -set BS=BS8 -set PPBS=BS7 -set PBS=BST78 ${MEMPOOL} ${MILLIGAS} ${FA2}
PROTO9 = ${MPP} ${STORAGEDIFF} ${META3} ${META4} -set PROTO=009_PsFLoren -set PROTONEXT=009_PsFLoren -set PROTONAME=009-PsFLoren -set __PROTO9__ -set BS=BS9 -set PPBS=BS8 -set PBS=BST89 ${MEMPOOL} ${MILLIGAS} ${FA2}
PROTO10 = ${MPP} ${STORAGEDIFF} ${META3} ${META4} ${META5} -set PROTO=010_PtGRANAD -set PROTONEXT=010_PtGRANAD -set PROTONAME=010-PtGRANAD -set __PROTO10__ -set BS=BS10 -set PPBS=BS9 -set PBS=BST910 ${MEMPOOL} ${MILLIGAS} ${FA2}
PROTO11 = ${MPP} ${STORAGEDIFF} ${META3} ${META4} ${META5} ${META6} -set PROTO=011_PtHangz2 -set PROTONEXT=011_PtHangz2 -set PROTONAME=011-PtHangz2 -set __PROTO11__ -set BS=BS11 -set PPBS=BS10 -set PBS=BST1011 ${MEMPOOL} ${MILLIGAS} ${FA2}
PROTO12 = ${MPP} ${STORAGEDIFF} ${META3} ${META4} ${META5} ${META6} ${META7} -set PROTO=012_Psithaca -set PROTONEXT=012_Psithaca -set PROTONAME=012-Psithaca -set __PROTO12__ -set BS=BS12 -set PPBS=BS11 -set PBS=BST1112 ${MEMPOOL} ${MILLIGAS} ${FA2}

ifdef ALPHA
PROTOA = ${MPP} ${STORAGEDIFF} ${META3} ${META4} ${META5} ${META6} ${META7} ${META8} -set PROTO=alpha -set PROTONEXT=alpha -set PROTONAME=alpha -set __PROTOA__ -set BS=BSA -set PPBS=BS11 -set PBS=BST12A ${MEMPOOL} ${MILLIGAS} ${FA2}
else
PROTOA = ${MPP} ${STORAGEDIFF} ${META3} ${META4} ${META5} ${META6} ${META7} ${META8} -set PROTO=alpha -set PROTONEXT=alpha -set PROTONAME=alpha -set __PROTOA__ -set BS=BSA -set PPBS=BS11 -set PBS=BST12A ${MEMPOOL} ${MILLIGAS} ${FA2} | cat > /dev/null
endif



# protocol transition blocks
TRANS = -set __TRANSITION__
TRANS_1_2 = ${PROTO1} ${TRANS} -set __TRANSITION_1_2__ -set PROTONEXT=002_PsYLVpVv -set BS=BST12 -set PBS=BS1
TRANS_2_3 = ${PROTO2} ${TRANS} -set __TRANSITION_2_3__ -set PROTONEXT=003_PsddFKi3 -set BS=BST23 -set PBS=BS2
TRANS_3_4 = ${PROTO3} ${TRANS} -set __TRANSITION_3_4__ -set PROTONEXT=004_Pt24m4xi -set BS=BST34 -set PBS=BS3
TRANS_4_5 = ${PROTO4} ${TRANS} -set __TRANSITION_4_5__ -set PROTONEXT=005_PsBabyM1 -set BS=BST45 -set PBS=BS4
TRANS_5_6 = ${PROTO5} ${TRANS} -set __TRANSITION_5_6__ -set PROTONEXT=006_PsCARTHA -set BS=BST56 -set PBS=BS5
TRANS_6_7 = ${PROTO6} ${TRANS} -set __TRANSITION_6_7__ -set PROTONEXT=007_PsDELPH1 -set BS=BST67 -set PBS=BS6
TRANS_7_8 = ${PROTO7} ${TRANS} -set __TRANSITION_7_8__ -set PROTONEXT=008_PtEdo2Zk -set BS=BST78 -set PBS=BS7
TRANS_8_9 = ${PROTO8} ${TRANS} -set __TRANSITION_8_9__ -set PROTONEXT=009_PsFLoren -set BS=BST89 -set PBS=BS8
TRANS_9_10 = ${PROTO9} ${TRANS} -set __TRANSITION_9_10__ -set PROTONEXT=010_PtGRANAD -set BS=BST910 -set PBS=BS9
TRANS_10_11 = ${PROTO10} ${TRANS} -set __TRANSITION_10_11__ -set PROTONEXT=011_PtHangz2 -set BS=BST1011 -set PBS=BS10
TRANS_11_12 = ${PROTO11} ${TRANS} -set __TRANSITION_11_12__ -set PROTONEXT=012_Psithaca -set BS=BST1112 -set PBS=BS11
TRANS_12_A = ${PROTO12} ${TRANS} -set __TRANSITION_12_A__ -set PROTONEXT=alpha -set BS=BST12A -set PBS=BS12


src/bin_indexer/indexer_helpers.ml:src/bin_indexer/indexer_helpers.ml.mpp Makefile
	sed -e 's/(\\\*#/(*/' < $< | ${MPP} > $@

ifdef ALPHA
src/bin_indexer/dune:src/bin_indexer/dune.mpp Makefile src/proto_alpha
	sed -e 's/(\\\*#/(*/' < $< | ${MPP} > $@
else
src/bin_indexer/dune:src/bin_indexer/dune.mpp Makefile
	sed -e 's/(\\\*#/(*/' < $< | ${MPP} > $@
endif

src/bin_indexer/snapshot_utils_6.ml:src/bin_indexer/snapshot_utils.ml.mpp Makefile
	${PROTO6} $< > $@
src/bin_indexer/snapshot_utils_6.mli:src/bin_indexer/snapshot_utils.mli.mpp Makefile
	${PROTO6} $< > $@

src/bin_indexer/snapshot_utils_7.ml:src/bin_indexer/snapshot_utils.ml.mpp Makefile
	${PROTO7} $< > $@
src/bin_indexer/snapshot_utils_7.mli:src/bin_indexer/snapshot_utils.mli.mpp Makefile
	${PROTO7} $< > $@

src/bin_indexer/snapshot_utils_8.ml:src/bin_indexer/snapshot_utils.ml.mpp Makefile
	${PROTO8} $< > $@
src/bin_indexer/snapshot_utils_8.mli:src/bin_indexer/snapshot_utils.mli.mpp Makefile
	${PROTO8} $< > $@

src/bin_indexer/snapshot_utils_9.ml:src/bin_indexer/snapshot_utils.ml.mpp Makefile
	${PROTO9} $< > $@
src/bin_indexer/snapshot_utils_9.mli:src/bin_indexer/snapshot_utils.mli.mpp Makefile
	${PROTO9} $< > $@

src/bin_indexer/snapshot_utils_10.ml:src/bin_indexer/snapshot_utils.ml.mpp Makefile
	${PROTO10} $< > $@
src/bin_indexer/snapshot_utils_10.mli:src/bin_indexer/snapshot_utils.mli.mpp Makefile
	${PROTO10} $< > $@

src/bin_indexer/snapshot_utils_11.ml:src/bin_indexer/snapshot_utils.ml.mpp Makefile
	${PROTO11} $< > $@
src/bin_indexer/snapshot_utils_11.mli:src/bin_indexer/snapshot_utils.mli.mpp Makefile
	${PROTO11} $< > $@

src/bin_indexer/snapshot_utils_12.ml:src/bin_indexer/snapshot_utils.ml.mpp Makefile
	${PROTO12} $< > $@
src/bin_indexer/snapshot_utils_12.mli:src/bin_indexer/snapshot_utils.mli.mpp Makefile
	${PROTO12} $< > $@

src/bin_indexer/snapshot_utils_A.ml:src/bin_indexer/snapshot_utils.ml.mpp Makefile
	cat $< | ${PROTOA} > $@
src/bin_indexer/snapshot_utils_A.mli:src/bin_indexer/snapshot_utils.mli.mpp Makefile
	cat $< | ${PROTOA} > $@


src/proto_001_PtCJ7pwo:src/proto_meta/* Makefile
	mkdir -p $@ ; touch $@
	sed -e 's/(\\\*#/(*/' src/proto_meta/chain_db.ml  | ${PROTO1} > $@/chain_db.ml
	sed -e 's/(\\\*#/(*/' src/proto_meta/chain_db.mli | ${PROTO1} > $@/chain_db.mli
	sed -e 's/(\\\*#/(*/' src/proto_meta/db_alpha.ml  | ${PROTO1} > $@/db_alpha.ml
	sed -e 's/(\\\*#/(*/' src/proto_meta/db_alpha.mli | ${PROTO1} > $@/db_alpha.mli
	sed -e 's/(\\\*#/(*/' src/proto_meta/contract_utils.ml | ${PROTO1} > $@/contract_utils.ml
	${PROTO1} src/proto_meta/dune-tpl > $@/dune
	cp src/proto_meta/tpl-opam $@/tezos-indexer-001-PtCJ7pwo.opam
	echo '(lang dune 1.11)' > $@/dune-project

src/proto_002_PsYLVpVv:src/proto_meta/* Makefile
	mkdir -p $@ ; touch $@
	sed -e 's/(\\\*#/(*/' src/proto_meta/chain_db.ml  | ${PROTO2} >  $@/chain_db.ml
	sed -e 's/(\\\*#/(*/' src/proto_meta/chain_db.mli | ${PROTO2} >  $@/chain_db.mli
	sed -e 's/(\\\*#/(*/' src/proto_meta/db_alpha.ml  | ${PROTO2} >  $@/db_alpha.ml
	sed -e 's/(\\\*#/(*/' src/proto_meta/db_alpha.mli | ${PROTO2} >  $@/db_alpha.mli
	sed -e 's/(\\\*#/(*/' src/proto_meta/contract_utils.ml | ${PROTO2} > $@/contract_utils.ml
	${PROTO2} src/proto_meta/dune-tpl > $@/dune
	cp src/proto_meta/tpl-opam $@/tezos-indexer-002-PsYLVpVv.opam
	echo '(lang dune 1.11)' > $@/dune-project

src/proto_003_PsddFKi3:src/proto_meta/* Makefile
	mkdir -p $@ ; touch $@
	sed -e 's/(\\\*#/(*/' src/proto_meta/chain_db.ml  | ${PROTO3} >  $@/chain_db.ml
	sed -e 's/(\\\*#/(*/' src/proto_meta/chain_db.mli | ${PROTO3} >  $@/chain_db.mli
	sed -e 's/(\\\*#/(*/' src/proto_meta/db_alpha.ml  | ${PROTO3} >  $@/db_alpha.ml
	sed -e 's/(\\\*#/(*/' src/proto_meta/db_alpha.mli | ${PROTO3} >  $@/db_alpha.mli
	sed -e 's/(\\\*#/(*/' src/proto_meta/contract_utils.ml | ${PROTO3} > $@/contract_utils.ml
	${PROTO3} src/proto_meta/dune-tpl > $@/dune
	cp src/proto_meta/tpl-opam $@/tezos-indexer-003-PsddFKi3.opam
	echo '(lang dune 1.11)' > $@/dune-project

src/proto_004_Pt24m4xi:src/proto_meta/* Makefile
	mkdir -p $@ ; touch $@
	sed -e 's/(\\\*#/(*/' src/proto_meta/chain_db.ml  | ${PROTO4} >  $@/chain_db.ml
	sed -e 's/(\\\*#/(*/' src/proto_meta/chain_db.mli | ${PROTO4} >  $@/chain_db.mli
	sed -e 's/(\\\*#/(*/' src/proto_meta/db_alpha.ml  | ${PROTO4} >  $@/db_alpha.ml
	sed -e 's/(\\\*#/(*/' src/proto_meta/db_alpha.mli | ${PROTO4} >  $@/db_alpha.mli
	sed -e 's/(\\\*#/(*/' src/proto_meta/contract_utils.ml | ${PROTO4} > $@/contract_utils.ml
	${PROTO4} src/proto_meta/dune-tpl > $@/dune
	cp src/proto_meta/tpl-opam $@/tezos-indexer-004-Pt24m4xi.opam
	echo '(lang dune 1.11)' > $@/dune-project

src/proto_005_PsBabyM1:src/proto_meta/* Makefile
	mkdir -p $@ ; touch $@
	sed -e 's/(\\\*#/(*/' src/proto_meta/chain_db.ml  | ${PROTO5} > $@/chain_db.ml
	sed -e 's/(\\\*#/(*/' src/proto_meta/chain_db.mli | ${PROTO5} > $@/chain_db.mli
	sed -e 's/(\\\*#/(*/' src/proto_meta/db_alpha.ml  | ${PROTO5} > $@/db_alpha.ml
	sed -e 's/(\\\*#/(*/' src/proto_meta/db_alpha.mli | ${PROTO5} > $@/db_alpha.mli
	sed -e 's/(\\\*#/(*/' src/proto_meta/contract_utils.ml | ${PROTO5} > $@/contract_utils.ml
	${PROTO5} src/proto_meta/dune-tpl > $@/dune
	cp src/proto_meta/tpl-opam $@/tezos-indexer-005-PsBabyM1.opam
	echo '(lang dune 1.11)' > $@/dune-project

src/proto_006_PsCARTHA:src/proto_meta/* Makefile
	mkdir -p $@ ; touch $@
	sed -e 's/(\\\*#/(*/' src/proto_meta/chain_db.ml  | ${PROTO6} > $@/chain_db.ml
	sed -e 's/(\\\*#/(*/' src/proto_meta/chain_db.mli | ${PROTO6} > $@/chain_db.mli
	sed -e 's/(\\\*#/(*/' src/proto_meta/db_alpha.ml  | ${PROTO6} > $@/db_alpha.ml
	sed -e 's/(\\\*#/(*/' src/proto_meta/db_alpha.mli | ${PROTO6} > $@/db_alpha.mli
	sed -e 's/(\\\*#/(*/' src/proto_meta/contract_utils.ml | ${PROTO6} > $@/contract_utils.ml
	sed -e 's/(\\\*#/(*/' src/proto_meta/mempool_utils.ml | ${PROTO6} > $@/mempool_utils.ml
	sed -e 's/(\\\*#/(*/' src/proto_meta/proto_fa2.ml | ${PROTO6} > $@/proto_fa2.ml
	${PROTO6} src/proto_meta/dune-tpl > $@/dune
	cp src/proto_meta/tpl-opam $@/tezos-indexer-006-PsCARTHA.opam
	echo '(lang dune 1.11)' > $@/dune-project

src/proto_007_PsDELPH1:src/proto_meta/* Makefile
	mkdir -p $@ ; touch $@
	sed -e 's/(\\\*#/(*/' src/proto_meta/chain_db.ml  | ${PROTO7} > $@/chain_db.ml
	sed -e 's/(\\\*#/(*/' src/proto_meta/chain_db.mli | ${PROTO7} > $@/chain_db.mli
	sed -e 's/(\\\*#/(*/' src/proto_meta/db_alpha.ml  | ${PROTO7} > $@/db_alpha.ml
	sed -e 's/(\\\*#/(*/' src/proto_meta/db_alpha.mli | ${PROTO7} > $@/db_alpha.mli
	sed -e 's/(\\\*#/(*/' src/proto_meta/contract_utils.ml | ${PROTO7} > $@/contract_utils.ml
	sed -e 's/(\\\*#/(*/' src/proto_meta/mempool_utils.ml | ${PROTO7} > $@/mempool_utils.ml
	sed -e 's/(\\\*#/(*/' src/proto_meta/proto_fa2.ml | ${PROTO7} > $@/proto_fa2.ml
	${PROTO7} src/proto_meta/dune-tpl > $@/dune
	cp src/proto_meta/tpl-opam $@/tezos-indexer-007-PsDELPH1.opam
	echo '(lang dune 1.11)' > $@/dune-project

src/proto_008_PtEdo2Zk:src/proto_meta/* Makefile
	mkdir -p $@ ; touch $@
	sed -e 's/(\\\*#/(*/' src/proto_meta/chain_db.ml  | ${PROTO8} > $@/chain_db.ml
	sed -e 's/(\\\*#/(*/' src/proto_meta/chain_db.mli | ${PROTO8} > $@/chain_db.mli
	sed -e 's/(\\\*#/(*/' src/proto_meta/db_alpha.ml  | ${PROTO8} > $@/db_alpha.ml
	sed -e 's/(\\\*#/(*/' src/proto_meta/db_alpha.mli | ${PROTO8} > $@/db_alpha.mli
	sed -e 's/(\\\*#/(*/' src/proto_meta/contract_utils.ml | ${PROTO8} > $@/contract_utils.ml
	sed -e 's/(\\\*#/(*/' src/proto_meta/mempool_utils.ml | ${PROTO8} > $@/mempool_utils.ml
	sed -e 's/(\\\*#/(*/' src/proto_meta/proto_fa2.ml | ${PROTO8} > $@/proto_fa2.ml
	${PROTO8} src/proto_meta/dune-tpl > $@/dune
	cp src/proto_meta/tpl-opam $@/tezos-indexer-008-PtEdo2Zk.opam
	echo '(lang dune 1.11)' > $@/dune-project

src/proto_009_PsFLoren:src/proto_meta/* Makefile
	mkdir -p $@ ; touch $@
	sed -e 's/(\\\*#/(*/' src/proto_meta/chain_db.ml  | ${PROTO9} > $@/chain_db.ml
	sed -e 's/(\\\*#/(*/' src/proto_meta/chain_db.mli | ${PROTO9} > $@/chain_db.mli
	sed -e 's/(\\\*#/(*/' src/proto_meta/db_alpha.ml  | ${PROTO9} > $@/db_alpha.ml
	sed -e 's/(\\\*#/(*/' src/proto_meta/db_alpha.mli | ${PROTO9} > $@/db_alpha.mli
	sed -e 's/(\\\*#/(*/' src/proto_meta/contract_utils.ml | ${PROTO9} > $@/contract_utils.ml
	sed -e 's/(\\\*#/(*/' src/proto_meta/mempool_utils.ml | ${PROTO9} > $@/mempool_utils.ml
	sed -e 's/(\\\*#/(*/' src/proto_meta/proto_fa2.ml | ${PROTO9} > $@/proto_fa2.ml
	${PROTO9} src/proto_meta/dune-tpl > $@/dune
	cp src/proto_meta/tpl-opam $@/tezos-indexer-009-PsFLoren.opam
	echo '(lang dune 1.11)' > $@/dune-project

src/proto_010_PtGRANAD:src/proto_meta/* Makefile
	mkdir -p $@ ; touch $@
	sed -e 's/(\\\*#/(*/' src/proto_meta/chain_db.ml  | ${PROTO10} > $@/chain_db.ml
	sed -e 's/(\\\*#/(*/' src/proto_meta/chain_db.mli | ${PROTO10} > $@/chain_db.mli
	sed -e 's/(\\\*#/(*/' src/proto_meta/db_alpha.ml  | ${PROTO10} > $@/db_alpha.ml
	sed -e 's/(\\\*#/(*/' src/proto_meta/db_alpha.mli | ${PROTO10} > $@/db_alpha.mli
	sed -e 's/(\\\*#/(*/' src/proto_meta/contract_utils.ml | ${PROTO10} > $@/contract_utils.ml
	sed -e 's/(\\\*#/(*/' src/proto_meta/mempool_utils.ml | ${PROTO10} > $@/mempool_utils.ml
	sed -e 's/(\\\*#/(*/' src/proto_meta/proto_fa2.ml | ${PROTO10} > $@/proto_fa2.ml
	${PROTO10} src/proto_meta/dune-tpl > $@/dune
	cp src/proto_meta/tpl-opam $@/tezos-indexer-010-PtGRANAD.opam
	echo '(lang dune 1.11)' > $@/dune-project

src/proto_011_PtHangz2:src/proto_meta/* Makefile
	mkdir -p $@ ; touch $@
	sed -e 's/(\\\*#/(*/' src/proto_meta/chain_db.ml  | ${PROTO11} > $@/chain_db.ml
	sed -e 's/(\\\*#/(*/' src/proto_meta/chain_db.mli | ${PROTO11} > $@/chain_db.mli
	sed -e 's/(\\\*#/(*/' src/proto_meta/db_alpha.ml  | ${PROTO11} > $@/db_alpha.ml
	sed -e 's/(\\\*#/(*/' src/proto_meta/db_alpha.mli | ${PROTO11} > $@/db_alpha.mli
	sed -e 's/(\\\*#/(*/' src/proto_meta/contract_utils.ml | ${PROTO11} > $@/contract_utils.ml
	sed -e 's/(\\\*#/(*/' src/proto_meta/mempool_utils.ml | ${PROTO11} > $@/mempool_utils.ml
	sed -e 's/(\\\*#/(*/' src/proto_meta/proto_fa2.ml | ${PROTO11} > $@/proto_fa2.ml
	${PROTO11} src/proto_meta/dune-tpl > $@/dune
	cp src/proto_meta/tpl-opam $@/tezos-indexer-011-PtHangz2.opam
	echo '(lang dune 1.11)' > $@/dune-project

src/proto_012_Psithaca:src/proto_meta/* Makefile
	mkdir -p $@ ; touch $@
	sed -e 's/(\\\*#/(*/' src/proto_meta/chain_db.ml  | ${PROTO12} > $@/chain_db.ml
	sed -e 's/(\\\*#/(*/' src/proto_meta/chain_db.mli | ${PROTO12} > $@/chain_db.mli
	sed -e 's/(\\\*#/(*/' src/proto_meta/db_alpha.ml  | ${PROTO12} > $@/db_alpha.ml
	sed -e 's/(\\\*#/(*/' src/proto_meta/db_alpha.mli | ${PROTO12} > $@/db_alpha.mli
	sed -e 's/(\\\*#/(*/' src/proto_meta/contract_utils.ml | ${PROTO12} > $@/contract_utils.ml
	sed -e 's/(\\\*#/(*/' src/proto_meta/mempool_utils.ml | ${PROTO12} > $@/mempool_utils.ml
	sed -e 's/(\\\*#/(*/' src/proto_meta/proto_fa2.ml | ${PROTO12} > $@/proto_fa2.ml
	${PROTO12} src/proto_meta/dune-tpl > $@/dune
	cp src/proto_meta/tpl-opam $@/tezos-indexer-012-Psithaca.opam
	echo '(lang dune 1.12)' > $@/dune-project

ifdef ALPHA
src/proto_alpha:src/proto_meta/* Makefile
	mkdir -p $@ ; touch $@
	sed -e 's/(\\\*#/(*/' src/proto_meta/chain_db.ml  | ${PROTOA} > $@/chain_db.ml
	sed -e 's/(\\\*#/(*/' src/proto_meta/chain_db.mli | ${PROTOA} > $@/chain_db.mli
	sed -e 's/(\\\*#/(*/' src/proto_meta/db_alpha.ml  | ${PROTOA} > $@/db_alpha.ml
	sed -e 's/(\\\*#/(*/' src/proto_meta/db_alpha.mli | ${PROTOA} > $@/db_alpha.mli
	sed -e 's/(\\\*#/(*/' src/proto_meta/contract_utils.ml | ${PROTOA} > $@/contract_utils.ml
	sed -e 's/(\\\*#/(*/' src/proto_meta/mempool_utils.ml | ${PROTOA} > $@/mempool_utils.ml
	sed -e 's/(\\\*#/(*/' src/proto_meta/proto_fa2.ml | ${PROTOA} > $@/proto_fa2.ml
	cat src/proto_meta/dune-tpl | ${PROTOA} > $@/dune
	cp src/proto_meta/tpl-opam $@/tezos-indexer-alpha.opam
	echo '(lang dune 1.11)' > $@/dune-project
else
src/proto_alpha:
	rm -fr src/proto_alpha
	echo "export ALPHA if you want support for proto alpha"
endif

src/bin_indexer/chain_db_transition_001_002.ml:src/proto_meta/chain_db.ml Makefile
	sed -e 's/(\\\*#/(*/' $< | ${TRANS_1_2} > $@

src/bin_indexer/chain_db_transition_001_002.mli:src/proto_meta/chain_db.mli Makefile
	sed -e 's/(\\\*#/(*/' $< | ${TRANS_1_2} > $@

src/bin_indexer/chain_db_transition_002_003.ml:src/proto_meta/chain_db.ml Makefile
	sed -e 's/(\\\*#/(*/' $< | ${TRANS_2_3} >  $@

src/bin_indexer/chain_db_transition_002_003.mli:src/proto_meta/chain_db.mli Makefile
	sed -e 's/(\\\*#/(*/' $< | ${TRANS_2_3} >  $@

src/bin_indexer/chain_db_transition_003_004.ml:src/proto_meta/chain_db.ml Makefile
	sed -e 's/(\\\*#/(*/' $< | ${TRANS_3_4} >  $@

src/bin_indexer/chain_db_transition_003_004.mli:src/proto_meta/chain_db.mli Makefile
	sed -e 's/(\\\*#/(*/' $< | ${TRANS_3_4} >  $@

src/bin_indexer/chain_db_transition_004_005.ml:src/proto_meta/chain_db.ml Makefile
	sed -e 's/(\\\*#/(*/' $< | ${TRANS_4_5} >  $@

src/bin_indexer/chain_db_transition_004_005.mli:src/proto_meta/chain_db.mli Makefile
	sed -e 's/(\\\*#/(*/' $< | ${TRANS_4_5} >  $@

src/bin_indexer/chain_db_transition_005_006.ml:src/proto_meta/chain_db.ml Makefile
	sed -e 's/(\\\*#/(*/' $< | ${TRANS_5_6} > $@

src/bin_indexer/chain_db_transition_005_006.mli:src/proto_meta/chain_db.mli Makefile
	sed -e 's/(\\\*#/(*/' $< | ${TRANS_5_6} > $@

src/bin_indexer/chain_db_transition_006_007.ml:src/proto_meta/chain_db.ml Makefile
	sed -e 's/(\\\*#/(*/' $< | ${TRANS_6_7} > $@

src/bin_indexer/chain_db_transition_006_007.mli:src/proto_meta/chain_db.mli Makefile
	sed -e 's/(\\\*#/(*/' $< | ${TRANS_6_7} > $@

src/bin_indexer/chain_db_transition_007_008.ml:src/proto_meta/chain_db.ml Makefile
	sed -e 's/(\\\*#/(*/' $< | ${TRANS_7_8} > $@

src/bin_indexer/chain_db_transition_007_008.mli:src/proto_meta/chain_db.mli Makefile
	sed -e 's/(\\\*#/(*/' $< | ${TRANS_7_8} > $@

src/bin_indexer/chain_db_transition_008_009.ml:src/proto_meta/chain_db.ml Makefile
	sed -e 's/(\\\*#/(*/' $< | ${TRANS_8_9} > $@

src/bin_indexer/chain_db_transition_008_009.mli:src/proto_meta/chain_db.mli Makefile
	sed -e 's/(\\\*#/(*/' $< | ${TRANS_8_9} > $@

src/bin_indexer/chain_db_transition_009_010.ml:src/proto_meta/chain_db.ml Makefile
	sed -e 's/(\\\*#/(*/' $< | ${TRANS_9_10} > $@

src/bin_indexer/chain_db_transition_009_010.mli:src/proto_meta/chain_db.mli Makefile
	sed -e 's/(\\\*#/(*/' $< | ${TRANS_9_10} > $@

src/bin_indexer/chain_db_transition_010_011.ml:src/proto_meta/chain_db.ml Makefile
	sed -e 's/(\\\*#/(*/' $< | ${TRANS_10_11} > $@

src/bin_indexer/chain_db_transition_010_011.mli:src/proto_meta/chain_db.mli Makefile
	sed -e 's/(\\\*#/(*/' $< | ${TRANS_10_11} > $@

src/bin_indexer/chain_db_transition_011_012.ml:src/proto_meta/chain_db.ml Makefile
	sed -e 's/(\\\*#/(*/' $< | ${TRANS_11_12} > $@

src/bin_indexer/chain_db_transition_011_012.mli:src/proto_meta/chain_db.mli Makefile
	sed -e 's/(\\\*#/(*/' $< | ${TRANS_11_12} > $@

src/bin_indexer/chain_db_transition_012_A.ml:src/proto_meta/chain_db.ml Makefile
	sed -e 's/(\\\*#/(*/' $< | ${TRANS_12_A} > $@

src/bin_indexer/chain_db_transition_012_A.mli:src/proto_meta/chain_db.mli Makefile
	sed -e 's/(\\\*#/(*/' $< | ${TRANS_12_A} > $@

src/bin_indexer/mempool_utils_transition_011_012.ml:src/proto_meta/mempool_utils.ml Makefile
	sed -e 's/(\\\*#/(*/' $< | ${TRANS_11_12} > $@

src/bin_indexer/mempool_utils_transition_012_A.ml:src/proto_meta/mempool_utils.ml Makefile
	sed -e 's/(\\\*#/(*/' $< | ${TRANS_12_A} > $@


clean:
	find . -name '*~' -delete
	rm -fr tezos-indexer $(GENERATED_FILES)
	rm -fr _build src/bin_indexer/.merlin src/bin_indexer/tezos-indexer.install src/lib_indexer/.merlin src/lib_indexer/tezos-indexer-lib.install

test-psql:
	bash utils/test-psql.bash



update-README-help:
	( IFS= ; while read l ; do \
	   echo "$$l" ; \
	   if grep -q '<!-- --help -- do not manually edit this or after this line -->' <<< "$$l"; \
	   then break; \
	   fi; \
	done < README.md ; echo '```'; ./tezos-indexer --help=plain ; echo '```') | sed -e 's/ *$$//' > README.md.tmp ; mv README.md.tmp README.md

.PHONY: clean protos test-psql db-schema-all-heavy db-schema-all-light db-schema-mezos db-schema-mezos-base db-schema-mezos-extra db-schema-all-default db-schema
