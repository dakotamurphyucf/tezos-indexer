image:
  name: gcr.io/kaniko-project/executor:debug
  entrypoint: [""]

variables:
  REGISTRY_NAME: "${CI_REGISTRY}/${CI_PROJECT_PATH}"
  CONTAINER_NAME: "${CI_REGISTRY}/${CI_PROJECT_PATH}/${CI_PROJECT_NAME}"
  KANIKO_CONFIG: "{\"auths\":{\"${CI_REGISTRY}\":{\"username\":\"${CI_REGISTRY_USER}\",\"password\":\"${CI_REGISTRY_PASSWORD}\"}}}"

stages:
  - build
  - test-pgsql-schema
  - test-pgsql12-schema
  - check-dev
  - check-non-dev
  - check-tag
  - push

test-postgresql-schema:
  image: postgres:alpine
  stage: test-pgsql-schema
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
      when: always
    - if: $CI_COMMIT_TAG
      when: always
    - if: '$CI_COMMIT_BRANCH == "master"'
      when: always
    - when: manual
  script:
    - apk add make bash ocaml
    - >
      mkdir -p /var/lib/postgresql/data /run/postgresql
      && chown -R postgres:postgres /run/postgresql /var/lib/postgresql
      && chmod 0700 /var/lib/postgresql/data
    - >
      su - postgres -c 'initdb -D /var/lib/postgresql/data && pg_ctl start -D /var/lib/postgresql/data && createdb tmp'
      && make db-schema-all-multicore | su - postgres -c 'psql -a tmp'
      && make db-schema-all-default | su - postgres -c 'psql -a tmp'
      && su - postgres -c 'dropdb tmp && createdb tmp'
      && make db-schema-all-default | su - postgres -c 'psql -a tmp'

test-postgresql12-schema:
  image: postgres:12.7-alpine
  stage: test-pgsql12-schema
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
      when: always
    - if: $CI_COMMIT_TAG
      when: always
    - if: '$CI_COMMIT_BRANCH == "master"'
      when: always
    - when: manual
  script:
    - apk add make bash ocaml
    - >
      mkdir -p /var/lib/postgresql/data /run/postgresql
      && chown -R postgres:postgres /run/postgresql /var/lib/postgresql
      && chmod 0700 /var/lib/postgresql/data
    - >
      su - postgres -c 'initdb -D /var/lib/postgresql/data && pg_ctl start -D /var/lib/postgresql/data && createdb tmp'
      && make db-schema-all-multicore | su - postgres -c 'psql -a tmp'
      && make db-schema-all-default | su - postgres -c 'psql -a tmp'
      && su - postgres -c 'dropdb tmp && createdb tmp'
      && make db-schema-all-default | su - postgres -c 'psql -a tmp'

build-docker-image:
  stage: build
  rules:
    - if: '$CI_PIPELINE_SOURCE != "merge_request_event"'
      when: never
    - if: $CI_COMMIT_MESSAGE =~ /(WIP|wip)/
      when: manual
    - if: $CI_COMMIT_TAG
      when: never
    - if: '$CI_COMMIT_BRANCH == "master"'
      when: never
    - when: always
  script:
    - echo "${KANIKO_CONFIG}" >/kaniko/.docker/config.json
    - >
      /kaniko/executor
      --cache
      --cache-repo "$REGISTRY_NAME"/cache
      --context "$CI_PROJECT_DIR"
      --dockerfile "$CI_PROJECT_DIR"/Dockerfile
      --build-arg GIT_TAG="${CI_COMMIT_SHA}"
      --build-arg GIT_COMMIT="${CI_COMMIT_REF_NAME}"
      --no-push

do-check-dev:
  stage: check-dev
  rules:
    - if: '$CI_PIPELINE_SOURCE != "merge_request_event"'
      when: never
    - if: '$CI_COMMIT_BRANCH == "master"'
      when: never
    - if: $CI_COMMIT_TAG
      when: never
    - if: $CI_COMMIT_MESSAGE =~ /(fix|Fix|update|Update|upgrade|Upgrade|release|Release)/
      when: never
    - when: on_success
  script:
    - grep -q 'true -- dev' src/db-schema/versions.sql

do-check-non-dev:
  stage: check-non-dev
  rules:
    - if: '$CI_COMMIT_BRANCH == "master"'
      when: on_success
    - if: $CI_COMMIT_TAG
      when: on_success
    - when: never
  script:
    - grep -qE '(false -- dev|true -- dev beta)' src/db-schema/versions.sql

do-check-tag:
  stage: check-tag
  rules:
    - if: $CI_COMMIT_TAG
      when: always
    - when: never
  script:
    - grep -q "$(echo "$CI_COMMIT_TAG" | sed -e 's/^v//' -e 's/rc.*//' -e 's/beta.*//')' -- version" src/db-schema/versions.sql

do-push:
  stage: push
  rules:
    - if: $CI_COMMIT_MESSAGE =~ /(WIP|wip)/
      when: manual
    - if: '$CI_COMMIT_BRANCH == "master"'
      when: manual
    - if: $CI_COMMIT_TAG
      when: on_success
  script:
    - echo "${KANIKO_CONFIG}" >/kaniko/.docker/config.json
    - >
      /kaniko/executor
      --cache
      --cache-repo "$REGISTRY_NAME"/cache
      --context "$CI_PROJECT_DIR"
      --dockerfile "$CI_PROJECT_DIR"/Dockerfile
      --destination "$CONTAINER_NAME":"$CI_COMMIT_REF_NAME"
      --destination "$CONTAINER_NAME":"latest"
      --build-arg GIT_TAG="${CI_COMMIT_SHA}"
      --build-arg GIT_COMMIT="${CI_COMMIT_REF_NAME}"
