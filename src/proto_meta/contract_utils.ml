(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2021 Nomadic Labs, <contact@nomadic-labs.com>               *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(* open Legacy_monad_globals *)

let proto = "(**# get PROTO #**)"

(**#X ifdef __META1__ open Tezos_raw_protocol_(**# get PROTO#**)X#**)
(**#X else open Tezos_protocol_(**# get PROTO #**) open Protocol X#**)
open Alpha_context
module Client = Tezos_client_(**# get PROTO #**)

class type rpc_context = (**#X ifdef __META1__ Client.Alpha_client_context.full
     X#**)(**#X else Client.Protocol_client_context.full X#**)

module FA12 = struct

  type action =
    (**#X ifdef __META_34__ Client.Client_proto_fa12.action = X#**)
    (**#X elseifdef __PROTO7__ Client.Client_proto_fa12.action = X#**)
    (**#X elseifdef __PROTO6__ Client.Client_proto_fa12.action = X#**)
    | Transfer of Contract.t * Contract.t * Z.t
    | Approve of Contract.t * Z.t
    | Get_allowance of Contract.t * Contract.t * (Contract.t * string option)
    | Get_balance of Contract.t * (Contract.t * string option)
    | Get_total_supply of (Contract.t * string option)

  let check_contract cctxt ~chain ~block ~contract () =
    let cctxt = (cctxt : #rpc_context :> rpc_context) in
    (**#X ifdef __META3__
       let res =
       Client.Client_proto_fa12.contract_has_fa12_interface
        cctxt ~chain ~block ~contract () in
       X#**)
    (**#X elseifdef __PROTO7__
       let res =
       Client.Client_proto_fa12.contract_has_fa12_interface
        cctxt ~chain ~block ~contract () in
       X#**)
    (**#X elseifdef __PROTO6__
       let res =
       Client.Client_proto_fa12.contract_has_fa12_interface
        cctxt ~chain ~block ~contract () in
       X#**)
    (**#X else
       let res = (fun _ _ _ _ -> Legacy_monad_globals.fail "not implemented")
       cctxt chain block contract in
       X#**)
    res

  let _transfer src dst z = Transfer (src, dst, z)
  let _approve addr z = Approve (addr, z)
  let _get_allowance src dst cb = Get_allowance (src, dst, cb)
  let _get_balance addr cb = Get_balance (addr, cb)
  let _get_total_supply cb = Get_total_supply cb

  let action_of_data entrypoint data : (action, unit) result =
    let action entrypoint =
      (**#X ifdef __META_34__
         let open Tezos_client_(**# get PROTO #**) in
         match Script_repr.force_decode data with
         | exception exn ->
         Printf.eprintf "Exception\n%!"; raise exn
         | Error _ -> Error ()
         | Ok (**# ifdef __META5__ expr #**) (**# else (expr, _) #**) ->
         Verbose.printf ~vl:4 "Action: %s: %a\n%!" entrypoint Michelson_v1_printer.print_expr expr;
          Tezos_micheline.Micheline.root expr
          |> Client.Client_proto_fa12.action_of_expr ~entrypoint
          |> begin function
             | Error _ -> Error ()
             | Ok v -> Ok v
             end
         X#**)
      (**#X elseifdef __PROTO7__
         match Script_repr.force_decode data with
         | Error _ -> Error ()
         | Ok (expr, _) ->
          Tezos_micheline.Micheline.root expr
          |> Client.Client_proto_fa12.action_of_expr ~entrypoint
          |> begin function
             | Error _ -> Error ()
             | Ok v -> Ok v
             end
         X#**)
      (**#X elseifdef __PROTO6__
         match Script_repr.force_decode data with
         | Error _ -> Error ()
         | Ok (expr, _) ->
          Tezos_micheline.Micheline.root expr
          |> Client.Client_proto_fa12.action_of_expr ~entrypoint
          |> begin function
             | Error _ -> Error ()
             | Ok v -> Ok v
             end
         X#**)
      (**#X else (fun _ _ -> Error ()) entrypoint data  X#**)
    in
    match entrypoint with
    | Some etp -> action etp
    | None -> Error ()

end

module FA2 = struct

  type token_id = Z.t

  type token_amount = Z.t

  type callback_contract = Contract.t * string option

  type operator_update_action =
    (**#X ifdef __FA2__ Proto_fa2.operator_update_action = X#**)
    | Add_operator | Remove_operator

  type action =
    (**#X ifdef __FA2__ Proto_fa2.action = X#**)
    | Transfer of (Contract.t * (Contract.t * token_id * token_amount) list)
          list
    | Update_operators of
        (operator_update_action * Contract.t * Contract.t * token_id) list
    | Balance_of of (Contract.t * token_id) list * callback_contract

  let _transfer = Transfer ([])
  let _update_operators = Update_operators ([])
  let _balance_of cb = Balance_of ([], cb)

  let operator_update_action_to_string = function
    | Add_operator -> "add_operator"
    | Remove_operator -> "remove_operator"

  let iteri_es_transfers transfers f =
    let open Legacy_monad_globals in
    List.fold_left_es (fun i (source, dests) ->
        List.fold_left_es (fun i (dest, token_id, amount) ->
            f i source dest amount token_id >>=? fun () ->
            return (i + 1))
          i
          dests)
      0
      transfers
    >>=? fun _ -> return_unit

  let iteri_es_update_operators updates f =
    List.iteri_es (fun i (kind, owner, operator, token_id) ->
        f i kind owner operator token_id)
      updates

  let iteri_es_balance_of requests callback f =
    List.iteri_es (fun i (owner, token_id) ->
        f i owner token_id callback)
      requests

  let check_contract cctxt ~chain ~block ~contract () =
    let cctxt = (cctxt : #rpc_context :> rpc_context) in
    (**#X ifdef __FA2__
       let res =
       Proto_fa2.contract_has_fa2_interface
        cctxt ~chain ~block ~contract () in
       X#**)
    (**#X else
       let res = (fun _ _ _ _ -> Legacy_monad_globals.fail "not implemented")
       cctxt chain block contract in
       X#**)
    res

  let action_of_data entrypoint data : (action, unit) result =
    let action entrypoint =
      (**#X ifdef __FA2__
         let open Tezos_client_(**# get PROTO #**) in
         match Script_repr.force_decode data with
         | exception exn ->
         Printf.eprintf "Exception\n%!"; raise exn
         | Error _ -> Error ()
         | Ok (**# ifdef __META5__ expr #**) (**# else (expr, _) #**) ->
         Verbose.printf ~vl:4 "Action: %s: %a" entrypoint Michelson_v1_printer.print_expr expr;
          Tezos_micheline.Micheline.root expr
          |> Proto_fa2.action_of_expr ~entrypoint
          |> begin function
             | Error _ -> Error ()
             | Ok v -> Ok v
             end
         X#**)
      (**#X else (fun _ _ -> Error ()) entrypoint data  X#**)
    in
    match entrypoint with
    | Some etp -> action etp
    | None -> Error ()
end

module Tokens = struct

  let scan_for_address data record_address =
    let open Legacy_monad_globals in
    (**#X ifdef __FA2__
       let rec scan = function
      | Tezos_micheline.Micheline.Int _ -> return_unit
      | Tezos_micheline.Micheline.Bytes (_, b) ->
        begin
          match Data_encoding.Binary.of_bytes Contract.encoding b with
          | Ok addr -> record_address addr
          | Error _ -> return_unit
        end
      | String (_, s) -> Contract.of_b58check s >>?= record_address
      | Prim (_, _, l, _) | Seq (_, l) ->
        List.iter_es scan l
    in
    match Script_repr.force_decode data with
    | exception _ -> return_unit
    | Error _ -> return_unit
    | Ok (**# ifdef __META5__ expr #**) (**# else (expr, _) #**) ->
      Tezos_micheline.Micheline.root expr
      |> scan
      >>= fun _ -> return_unit
     X#**)
    (**#X else let _ = data, record_address in return_unit  X#**)

  type kind = FA12 | FA2

  let string_of_kind = function FA12 -> "fa1-2" | FA2 -> "fa2"

  let check_contract:
    _ -> chain:_ -> block:_ -> contract:_ -> unit -> (kind, _) result Lwt.t =
    fun cctxt ~chain ~block ~contract () ->
    let open Legacy_monad_globals in
    FA12.check_contract cctxt ~chain ~block ~contract ()
    >>= function
    | Ok () -> return FA12
    | Error _ ->
      FA2.check_contract cctxt ~chain ~block ~contract ()
      >>=? fun () -> return FA2

end
