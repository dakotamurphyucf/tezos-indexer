-- Open Source License
-- Copyright (c) 2020 Nomadic Labs <contact@nomadic-labs.com>
--
-- Permission is hereby granted, free of charge, to any person obtaining a
-- copy of this software and associated documentation files (the "Software"),
-- to deal in the Software without restriction, including without limitation
-- the rights to use, copy, modify, merge, publish, distribute, sublicense,
-- and/or sell copies of the Software, and to permit persons to whom the
-- Software is furnished to do so, subject to the following conditions:
--
-- The above copyright notice and this permission notice shall be included
-- in all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
-- THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
-- FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
-- DEALINGS IN THE SOFTWARE.

-- Lines starting with --OPT may be automatically activated
-- Lines ending with --OPT may be automatically deactivated

-- DB schema for tokens operations

SELECT 'tokens.sql' as file;

DO $$
BEGIN
  IF NOT EXISTS (SELECT 1 FROM pg_type WHERE typname = 'token_contract_kind') THEN
    CREATE TYPE token_contract_kind AS ENUM ('fa1-2', 'fa2');
  END IF;
END
$$;

CREATE TABLE IF NOT EXISTS T.contract ( --NL
  address_id bigint not null
, block_hash_id int not null
, kind token_contract_kind not null
-- , primary key(address_id)
--  , foreign key(block_hash_id) references C.block(hash_id) on delete cascade
);
--pkey contract_pkey; T.contract; address_id --SEQONLY
--FKEY t_contract_block_hash_id; T.contract; block_hash_id; C.block(hash_id); CASCADE --SEQONLY

select now (); --SEQONLY
DO $$
BEGIN
  IF (SELECT count(*) FROM pg_tables WHERE tablename = 'indexer_version') > 0
     AND (SELECT count(*) FROM indexer_version WHERE conversion_in_progress) > 0
  THEN

-- Do not use the following "temporary table method" because T.contract is a tiny table and the naive simple query returns pretty much instantaneously
-- CREATE TABLE IF NOT EXISTS T.contract_tmp ( --NL
--   address_id bigint primary key
-- , block_hash_id int not null
-- , kind token_contract_kind not null
-- );
-- CREATE INDEX IF NOT EXISTS token_contract_block_hash on T.contract using btree(block_hash_id);
-- INSERT INTO T.contract_tmp SELECT * FROM T.contract ORDER BY block_hash_id ASC ON CONFLICT DO NOTHING;
-- DROP TABLE T.contract CASCADE;
-- ALTER TABLE T.contract_tmp RENAME TO T.contract;

-- Here follows the simple query that returns pretty much instantaneously:
    DELETE FROM
      T.contract a
        USING T.contract b
      WHERE
        a.block_hash_id > b.block_hash_id
      AND a.address_id = b.address_id;
  END IF;
END;
$$;
DO $$
BEGIN
  IF  NOT ((SELECT count(*) FROM indexer_version WHERE multicore) > 0)
   OR (SELECT count(*) FROM indexer_version WHERE conversion_in_progress) > 0
  THEN
    BEGIN
     ALTER TABLE T.contract ADD CONSTRAINT contract_pkey PRIMARY KEY (address_id);
      EXCEPTION WHEN SQLSTATE '42P16' THEN RETURN;
    END;
  END IF;
END
$$;
select now (); --SEQONLY


CREATE OR REPLACE FUNCTION T.I_contract (a bigint, b int, k token_contract_kind)
returns void
as $$
insert into T.contract (address_id, block_hash_id, kind)
values (a, b, k)
on conflict do nothing; --SEQONLY
$$ language sql;

--CREATE INDEX IF NOT EXISTS token_contract_address on T.contract using btree(address_id); --SEQONLY --1
CREATE INDEX IF NOT EXISTS token_contract_block_hash on T.contract using btree(block_hash_id); --SEQONLY --1
--CREATE INDEX IF NOT EXISTS token_contract_autoid on T.contract using btree(autoid); --SEQONLY



-- CREATE TABLE IF NOT EXISTS T.balance (
--   token_address_id bigint not null
-- , address_id bigint not null
-- , amount smallint
-- , primary key (token_address_id, address_id)
-- );
-- --FKEY t_balance_token_address_id_fkey; T.balance; token_address_id; T.contract(address_id); CASCADE --SEQONLY
-- --FKEY t_balance_address_id_fkey; T.balance; address_id; C.addresses(address_id); CASCADE --SEQONLY

-- CREATE INDEX IF NOT EXISTS token_balance_token on T.balance using btree(token_address_id); --SEQONLY
-- CREATE INDEX IF NOT EXISTS token_balance_address on T.balance using btree(address_id); --SEQONLY
-- CREATE INDEX IF NOT EXISTS token_balance_autoid on T.balance using btree(autoid) ;--SEQONLY



-- CREATE OR REPLACE FUNCTION T.I_balance (ta bigint, a bigint, am smallint)
-- returns void
-- as $$
-- -- insert into addresses values (a) on conflict do nothing;
-- insert into T.balance (token_address_id, address_id, amount)
-- values (ta, a, am)
-- on conflict (token_address_id, address_id) do  --SEQONLY
-- update set amount = am where T.balance.token_address_id = ta and T.balance.address_id = a;  --SEQONLY
-- $$ language sql;

DO $$
BEGIN
  IF NOT EXISTS (SELECT 1 FROM pg_type WHERE typname = 'fa12_operation_kind') THEN
    CREATE TYPE fa12_operation_kind AS ENUM ('transfer', 'approve', 'getBalance', 'getAllowance', 'getTotalSupply');
  END IF;
END
$$;



CREATE TABLE IF NOT EXISTS T.fa12_operation ( --NL
  operation_id bigint not null primary key
, token_address_id bigint not null
, caller_id bigint not null
, kind fa12_operation_kind not null
, autoid SERIAL UNIQUE -- this field should always be last --SEQONLY
);
--FKEY t_fa12_operation_token_address_id_fkey; T.fa12_operation; token_address_id; T.contract(address_id); CASCADE --SEQONLY
--FKEY t_fa12_operation_operation_id_fkey; T.fa12_operation; operation_id; C.operation_alpha(autoid) ; CASCADE --SEQONLY
--FKEY t_fa12_operation_caller_id_fkey; T.fa12_operation; caller_id; C.addresses(address_id) ; CASCADE --SEQONLY
CREATE INDEX IF NOT EXISTS fa12_operation_kind    on T.fa12_operation using btree (kind); --SEQONLY --1
-- CREATE INDEX IF NOT EXISTS fa12_operation_autoid  on T.fa12_operation using btree (autoid); --SEQONLY
CREATE INDEX IF NOT EXISTS fa12_operation_caller  on T.fa12_operation using btree (caller_id); --SEQONLY --1
CREATE INDEX IF NOT EXISTS fa12_operation_address on T.fa12_operation using btree (token_address_id); --SEQONLY --1

WITH X AS (SELECT DISTINCT(token_address_id) FROM T.fa12_operation WHERE token_address_id NOT IN (SELECT address_id FROM T.contract)) --SEQONLY
INSERT INTO T.contract (address_id, block_hash_id) --SEQONLY
SELECT X.token_address_id, ((SELECT Y.operation_id FROM T.fa12_operation Y WHERE Y.token_address_id = X.token_address_id ORDER BY operation_id asc limit 1)/10000000::bigint)::int --SEQONLY
FROM X ; --SEQONLY


CREATE OR REPLACE FUNCTION T.I_fa12_operation (opaid bigint, ta bigint, c bigint, k fa12_operation_kind)
returns void
as $$
insert into T.fa12_operation
(operation_id, token_address_id, caller_id, kind)
values (opaid, ta, c, k)
on conflict (operation_id) do nothing;
--SEQONLY
$$ language sql;



CREATE TABLE IF NOT EXISTS T.fa12_transfer ( --L
  operation_id bigint not null primary key
, source_id bigint not null
, destination_id bigint not null
, amount numeric not null
);
--FKEY t_fa12_transfer_operation_id_fkey; T.fa12_transfer; operation_id; T.fa12_operation(operation_id) ; CASCADE --SEQONLY
--FKEY t_fa12_transfer_source_id_fkey; T.fa12_transfer; source_id; C.addresses(address_id) ; CASCADE --SEQONLY
--FKEY t_fa12_transfer_destination_id_fkey; T.fa12_transfer; destination_id; C.addresses(address_id) ; CASCADE --SEQONLY
CREATE INDEX IF NOT EXISTS fa12_transfer_source         on T.fa12_transfer using btree (source_id); --SEQONLY --1
CREATE INDEX IF NOT EXISTS fa12_transfer_destination    on T.fa12_transfer using btree (destination_id); --SEQONLY --1
-- CREATE INDEX IF NOT EXISTS fa12_transfer_autoid         on T.fa12_transfer using btree (autoid); --SEQONLY


CREATE OR REPLACE FUNCTION T.I_fa12_transfer (opaid bigint, s bigint, d bigint, a numeric)
returns void
as $$
insert into T.fa12_transfer (operation_id, source_id, destination_id, amount)
values (opaid, s, d, a)
on conflict (operation_id) do nothing; --SEQONLY
$$ language sql;



CREATE TABLE IF NOT EXISTS T.fa12_approve ( --L
  operation_id bigint not null primary key
, address_id bigint not null
, amount numeric not null
);
--FKEY t_fa12_approve_operation_id_fkey; T.fa12_approve; operation_id; T.fa12_operation(operation_id) ; CASCADE --SEQONLY
--FKEY t_fa12_approve_address_id_fkey; T.fa12_approve; address_id; C.addresses(address_id) ; CASCADE --SEQONLY
CREATE INDEX IF NOT EXISTS fa12_approve_address           on T.fa12_approve using btree (address_id); --SEQONLY --1
-- CREATE INDEX IF NOT EXISTS fa12_approve_autoid            on T.fa12_approve using btree (autoid); --SEQONLY



CREATE OR REPLACE FUNCTION T.I_fa12_approve(opaid bigint, a bigint, am numeric)
returns void
as $$
insert into T.fa12_approve (operation_id, address_id, amount)
values (opaid, a, am)
on conflict (operation_id) do nothing --SEQONLY
$$ language sql;


CREATE TABLE IF NOT EXISTS T.fa12_get_balance ( --L
  operation_id bigint not null primary key
, address_id bigint not null
, callback_id bigint not null
);
--FKEY t_fa12_get_balance_operation_id_fkey; T.fa12_get_balance; operation_id; T.fa12_operation(operation_id) ; CASCADE --SEQONLY
--FKEY t_fa12_get_balance_address_id_fkey; T.fa12_get_balance; address_id; C.addresses(address_id) ; CASCADE --SEQONLY
--FKEY t_fa12_get_balance_callback_id_fkey; T.fa12_get_balance; callback_id; C.addresses(address_id) ; CASCADE --SEQONLY
CREATE INDEX IF NOT EXISTS fa12_get_balance_address           on T.fa12_get_balance using btree (address_id); --SEQONLY --1
CREATE INDEX IF NOT EXISTS fa12_get_balance_callback          on T.fa12_get_balance using btree (callback_id); --SEQONLY --1
-- CREATE INDEX IF NOT EXISTS fa12_get_balance_autoid            on T.fa12_get_balance using btree (autoid); --SEQONLY


CREATE OR REPLACE FUNCTION T.I_fa12_get_balance (opaid bigint, a bigint, c bigint)
returns void
as $$
insert into T.fa12_get_balance(operation_id, address_id, callback_id)
values (opaid, a, c)
on conflict (operation_id) do nothing; --SEQONLY
$$ language sql;



CREATE TABLE IF NOT EXISTS T.fa12_get_allowance ( --L
  operation_id bigint not null primary key
, source_id bigint not null
, destination_id bigint not null
, callback_id bigint not null
);
--FKEY t_fa12_get_allowance_operation_id_fkey; T.fa12_get_allowance; operation_id; T.fa12_operation(operation_id) ; CASCADE --SEQONLY
--FKEY t_fa12_get_allowance_source_id_fkey; T.fa12_get_allowance; source_id; C.addresses(address_id) ; CASCADE --SEQONLY
--FKEY t_fa12_get_allowance_destination_id_fkey; T.fa12_get_allowance; destination_id; C.addresses(address_id) ; CASCADE --SEQONLY
--FKEY t_fa12_get_allowance_callback_id_fkey; T.fa12_get_allowance; callback_id; C.addresses(address_id) ; CASCADE --SEQONLY

CREATE INDEX IF NOT EXISTS fa12_get_allowance_source            on T.fa12_get_allowance using btree (source_id); --SEQONLY --1
CREATE INDEX IF NOT EXISTS fa12_get_allowance_destination       on T.fa12_get_allowance using btree (destination_id); --SEQONLY --1
CREATE INDEX IF NOT EXISTS fa12_get_allowance_callback          on T.fa12_get_allowance using btree (callback_id); --SEQONLY --1
-- CREATE INDEX IF NOT EXISTS fa12_get_allowance_autoid            on T.fa12_get_balance using btree (autoid); --SEQONLY


CREATE OR REPLACE FUNCTION T.I_fa12_get_allowance (opaid bigint, s bigint, d bigint, c bigint)
returns void
as $$
insert into T.fa12_get_allowance(operation_id, source_id, destination_id, callback_id)
values (opaid, s, d, c)
on conflict (operation_id) do nothing; --SEQONLY
$$ language sql;


CREATE TABLE IF NOT EXISTS T.fa12_get_total_supply ( --L
  operation_id bigint not null primary key
, callback_id bigint not null
);
--FKEY t_fa12_get_total_supply_operation_id_fkey; T.fa12_get_total_supply; operation_id; T.fa12_operation(operation_id) ; CASCADE --SEQONLY
--FKEY t_fa12_get_total_supply_callback_id_fkey; T.fa12_get_total_supply; callback_id; C.addresses(address_id) ; CASCADE --SEQONLY
CREATE INDEX IF NOT EXISTS fa12_get_total_supply_callback          on T.fa12_get_total_supply using btree (callback_id); --SEQONLY --1
-- CREATE INDEX IF NOT EXISTS fa12_get_total_supply_autoid            on T.fa12_get_total_supply using btree (autoid); --SEQONLY



CREATE OR REPLACE FUNCTION T.I_fa12_get_total_supply(opaid bigint, c bigint)
returns void
as $$
insert into T.fa12_get_total_supply(operation_id, callback_id)
values (opaid, c)
on conflict (operation_id) do nothing; --SEQONLY
$$ language sql;


---- FA2


DO $$
BEGIN
  IF NOT EXISTS (SELECT 1 FROM pg_type WHERE typname = 'fa2_operation_kind') THEN
    CREATE TYPE fa2_operation_kind AS ENUM ('transfer', 'update_operators', 'balance_of');
  END IF;
END
$$;


CREATE TABLE IF NOT EXISTS T.fa2_operation ( --NL
  operation_id bigint not null primary key
, token_address_id bigint not null
, caller_id bigint not null
, kind fa2_operation_kind not null
, autoid SERIAL UNIQUE -- this field should always be last --SEQONLY
);
--FKEY t_fa2_operation_token_address_id_fkey; T.fa2_operation; token_address_id; T.contract(address_id); CASCADE --SEQONLY
--FKEY t_fa2_operation_operation_id_fkey; T.fa2_operation; operation_id; C.operation_alpha(autoid) ; CASCADE --SEQONLY
--FKEY t_fa2_operation_caller_id_fkey; T.fa2_operation; caller_id; C.addresses(address_id) ; CASCADE --SEQONLY
CREATE INDEX IF NOT EXISTS fa2_operation_kind    on T.fa2_operation using btree (kind); --SEQONLY --1
-- CREATE INDEX IF NOT EXISTS fa2_operation_autoid  on T.fa2_operation using btree (autoid); --SEQONLY
CREATE INDEX IF NOT EXISTS fa2_operation_caller  on T.fa2_operation using btree (caller_id); --SEQONLY --1
CREATE INDEX IF NOT EXISTS fa2_operation_address on T.fa2_operation using btree (token_address_id); --SEQONLY --1

WITH X AS (SELECT DISTINCT(token_address_id) FROM T.fa2_operation WHERE token_address_id NOT IN (SELECT address_id FROM T.contract)) --SEQONLY
INSERT INTO T.contract (address_id, block_hash_id) --SEQONLY
SELECT X.token_address_id, ((SELECT Y.operation_id FROM T.fa2_operation Y WHERE Y.token_address_id = X.token_address_id ORDER BY operation_id asc limit 1)/10000000::bigint)::int --SEQONLY
FROM X ; --SEQONLY


CREATE OR REPLACE FUNCTION T.I_fa2_operation (opaid bigint, ta bigint, c bigint, k fa2_operation_kind)
returns void
as $$
insert into T.fa2_operation
(operation_id, token_address_id, caller_id, kind)
values (opaid, ta, c, k)
on conflict (operation_id) do nothing;
--SEQONLY
$$ language sql;


CREATE TABLE IF NOT EXISTS T.fa2_transfer ( --L
  operation_id bigint not null
, internal_op_id smallint not null
, token_id numeric not null
, source_id bigint not null
, destination_id bigint not null
, amount numeric not null
, PRIMARY KEY (operation_id, internal_op_id)
);
--FKEY t_fa2_transfer_operation_id_fkey; T.fa2_transfer; operation_id; T.fa2_operation(operation_id) ; CASCADE --SEQONLY
--FKEY t_fa2_transfer_source_id_fkey; T.fa2_transfer; source_id; C.addresses(address_id) ; CASCADE --SEQONLY
--FKEY t_fa2_transfer_destination_id_fkey; T.fa2_transfer; destination_id; C.addresses(address_id) ; CASCADE --SEQONLY
CREATE INDEX IF NOT EXISTS fa2_transfer_source         on T.fa2_transfer using btree (source_id); --SEQONLY --1
CREATE INDEX IF NOT EXISTS fa2_transfer_destination    on T.fa2_transfer using btree (destination_id); --SEQONLY --1
-- CREATE INDEX IF NOT EXISTS fa2_transfer_autoid         on T.fa2_transfer using btree (autoid); --SEQONLY


CREATE OR REPLACE FUNCTION T.I_fa2_transfer (opaid bigint, iop bigint, tkid numeric, s bigint, d bigint, a numeric)
returns void
as $$
insert into T.fa2_transfer (operation_id, internal_op_id, token_id, source_id, destination_id, amount)
values (opaid, iop, tkid, s, d, a)
on conflict (operation_id, internal_op_id) do nothing; --SEQONLY
$$ language sql;


CREATE TABLE IF NOT EXISTS T.fa2_update_operators ( --L
  operation_id bigint not null
, internal_op_id smallint not null
, token_id numeric not null
, add_operator bool not null
, owner_id bigint not null
, operator_id bigint not null
, PRIMARY KEY (operation_id, internal_op_id)
);
--FKEY t_fa2_update_operators_operation_id_fkey; T.fa2_update_operators; operation_id; T.fa2_operation(operation_id) ; CASCADE --SEQONLY
--FKEY t_fa2_update_operators_owner_id_fkey; T.fa2_update_operators; owner_id; C.addresses(address_id) ; CASCADE --SEQONLY
--FKEY t_fa2_update_operators_operator_id_fkey; T.fa2_update_operators; operator_id; C.addresses(address_id) ; CASCADE --SEQONLY
CREATE INDEX IF NOT EXISTS fa2_update_operators_owner    on T.fa2_update_operators using btree (owner_id); --SEQONLY --1
CREATE INDEX IF NOT EXISTS fa2_update_operators_operator on T.fa2_update_operators using btree (operator_id); --SEQONLY --1
-- CREATE INDEX IF NOT EXISTS fa2_update_operators_autoid   on T.fa2_update_operators using btree (autoid); --SEQONLY


CREATE OR REPLACE FUNCTION T.I_fa2_update_operators (opaid bigint, iop bigint, tkid numeric, add_op bool, ow bigint, op bigint)
returns void
as $$
insert into T.fa2_update_operators (operation_id, internal_op_id, token_id, add_operator, owner_id, operator_id)
values (opaid, iop, tkid, add_op, ow, op)
on conflict (operation_id, internal_op_id) do nothing; --SEQONLY
$$ language sql;


CREATE TABLE IF NOT EXISTS T.fa2_balance_of ( --L
  operation_id bigint not null
, internal_op_id smallint not null
, token_id numeric not null
, owner_id bigint not null
, callback_id bigint not null
, PRIMARY KEY (operation_id, internal_op_id)
);
--FKEY t_fa2_balance_of_operation_id_fkey; T.fa2_balance_of; operation_id; T.fa2_operation(operation_id) ; CASCADE --SEQONLY
--FKEY t_fa2_balance_of_owner_id_fkey; T.fa2_balance_of; owner_id; C.addresses(address_id) ; CASCADE --SEQONLY
--FKEY t_fa2_balance_of_callback_id_fkey; T.fa2_balance_of; callback_id; C.addresses(address_id) ; CASCADE --SEQONLY
CREATE INDEX IF NOT EXISTS fa2_balance_of_owner    on T.fa2_balance_of using btree (owner_id); --SEQONLY --1
CREATE INDEX IF NOT EXISTS fa2_balance_of_callback on T.fa2_balance_of using btree (callback_id); --SEQONLY --1
-- CREATE INDEX IF NOT EXISTS fa2_balance_of_autoid   on T.fa2_balance_of using btree (autoid); --SEQONLY


CREATE OR REPLACE FUNCTION T.I_fa2_balance_of (opaid bigint, iop bigint, tkid numeric, ow bigint, cb bigint)
returns void
as $$
insert into T.fa2_balance_of (operation_id, internal_op_id, token_id, owner_id, callback_id)
values (opaid, iop, tkid, ow, cb)
on conflict (operation_id, internal_op_id) do nothing; --SEQONLY
$$ language sql;


CREATE OR REPLACE FUNCTION T.is_token (t bigint)
returns table(kind token_contract_kind)
as $$
select kind from T.contract where address_id = t limit 1
-- the "limit 1" is only useful for multicore mode
$$ language sql stable;


CREATE TABLE IF NOT EXISTS T.accounts_registry ( --L
  token_address_id bigint not null
, address_id bigint not null
, primary key (token_address_id, address_id)
);

--fkey t_accounts_registry_token_address_id_fkey; T.accounts_registry; token_address_id; T.contract(address_id); CASCADE --SEQONLY
-- drop t_accounts_registry_token_address_id_fkey because of an obscure race condition
DO $$ -- since v9.7.a
BEGIN
ALTER TABLE t.accounts_registry DROP CONSTRAINT t_accounts_registry_token_address_id_fkey;
EXCEPTION WHEN OTHERS THEN
END $$;

--FKEY t_accounts_registry_token_address_id_fkey; T.accounts_registry; token_address_id; C.addresses(address_id); CASCADE --SEQONLY
--FKEY t_accounts_registry_address_id_fkey; T.accounts_registry; address_id; C.addresses(address_id); CASCADE --SEQONLY
CREATE INDEX IF NOT EXISTS token_accounts_registry_token on T.accounts_registry using btree(token_address_id); --SEQONLY --1
CREATE INDEX IF NOT EXISTS token_accounts_registry_address on T.accounts_registry using btree(address_id); --SEQONLY --1
-- CREATE INDEX IF NOT EXISTS token_accounts_registry_autoid on T.accounts_registry using btree(autoid); --SEQONLY

CREATE OR REPLACE FUNCTION T.I_accounts_registry (ta bigint, a bigint)
returns void
as $$
-- insert into addresses values (a) on conflict do nothing;
insert into T.accounts_registry (token_address_id, address_id)
values (ta, a)
on conflict (token_address_id, address_id) do nothing;
$$ language sql;

-- In multicore mode, we put the values in a temporary table instead, that will not care about duplicates, for faster insertion. Then we take the contents of the table and dump it without duplicates into the final table. This allows much faster insertions.
-- multicore mode only:
CREATE TABLE IF NOT EXISTS accounts_registry_tmp ( --MULTICORE
  token_address_id bigint not null --MULTICORE
, address_id bigint not null --MULTICORE
); --MULTICORE
CREATE OR REPLACE FUNCTION T.I_accounts_registry (ta bigint, a bigint) --MULTICORE
returns void --MULTICORE
as $$ --MULTICORE
insert into accounts_registry_tmp (token_address_id, address_id) --MULTICORE
values (ta, a) --MULTICORE
$$ language sql; --MULTICORE
-- convert from multicore to default mode:
DO $$ --SEQONLY
BEGIN --SEQONLY
  IF EXISTS (SELECT 1 FROM pg_tables where tablename = 'accounts_registry_tmp') THEN --SEQONLY
    INSERT INTO T.accounts_registry (token_address_id, address_id) --SEQONLY
    SELECT token_address_id, address_id FROM accounts_registry_tmp --SEQONLY
    ON CONFLICT DO NOTHING; --SEQONLY
    DROP TABLE accounts_registry_tmp; --SEQONLY
  END IF; --SEQONLY
END --SEQONLY
$$; --SEQONLY
