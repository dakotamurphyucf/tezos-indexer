-- Open Source License
-- Copyright (c) 2018-2021 Nomadic Labs <contact@nomadic-labs.com>
--
-- Permission is hereby granted, free of charge, to any person obtaining a
-- copy of this software and associated documentation files (the "Software"),
-- to deal in the Software without restriction, including without limitation
-- the rights to use, copy, modify, merge, publish, distribute, sublicense,
-- and/or sell copies of the Software, and to permit persons to whom the
-- Software is furnished to do so, subject to the following conditions:
--
-- The above copyright notice and this permission notice shall be included
-- in all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
-- THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
-- FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
-- DEALINGS IN THE SOFTWARE.

SELECT 'mainnet_block_validation.sql' as file;

CREATE OR REPLACE FUNCTION epoch_time_stamp (block_level int)
RETURNS int
AS $$
select extract(epoch from timestamp)::int from c.block where level = block_level;
$$ LANGUAGE SQL STABLE;


-- Disclaimer: The information provided on whether a block is final is
-- based on the analysis performed
-- [here](https://blog.nomadic-labs.com/emmy-in-the-partial-synchrony-model.html).
-- We emphasize that this analysis is based on a certain model of the
-- blockchain and of an attacker, which may not faithfully represent
-- the reality. In particular, the model assumes reasonable bounds on
-- the communication delays, namely that blocks and operations are
-- diffused within 30 seconds. The attacker model is that of a baker
-- not following the rules of the protocol, that is, it may double
-- bake and double endorse as it finds best in order to fork the
-- chain. Said otherwise, the attacker model does not capture an
-- attacker capable of disturbing the network by blocking or delaying
-- messages between honest participants. Finally, the underlying
-- computations are with respect to the security threshold of 10^-8
-- (that is, the chances of being wrong about a block being final are
-- one in 2 centuries) and of an attacker with at most 20% of the
-- total active stake.

CREATE OR REPLACE FUNCTION is_mainnet ()
RETURNS bool
AS $$
  select (select hash from c.chain) = 'NetXdQprcVkpaWU'
$$ LANGUAGE SQL STABLE;


-- This function is relevant for mainnet using Emmy+ or Emmy* consensus algorithm.
CREATE OR REPLACE FUNCTION mainnet_block_is_final (block_level int)
RETURNS bool
AS $$
DECLARE
  ct int := (SELECT epoch_time_stamp(block_level));
  max_level int := (SELECT level FROM c.block ORDER BY level DESC LIMIT 1);
BEGIN
  IF (block_level > 1589248) THEN
    IF (max_level - block_level) < 2 THEN RETURN false; END IF;
    IF ((SELECT epoch_time_stamp(block_level +  2)) - ct) <=   395 THEN RETURN true; END IF;
    IF ((SELECT epoch_time_stamp(block_level +  3)) - ct) <=   915 THEN RETURN true; END IF;
    IF ((SELECT epoch_time_stamp(block_level +  4)) - ct) <=  1448 THEN RETURN true; END IF;
    RETURN (select mainnet_block_is_final(block_level + 1));
  END IF;
  IF (max_level - block_level) < 3 THEN RETURN (select mainnet_block_is_final(block_level+1)); END IF;
  IF ((SELECT epoch_time_stamp(block_level +  3)) - ct) <=  227 AND block_level < 1589248 -  2 THEN RETURN true; END IF;
  IF ((SELECT epoch_time_stamp(block_level +  4)) - ct) <=  455 AND block_level < 1589248 -  3 THEN RETURN true; END IF;
  IF ((SELECT epoch_time_stamp(block_level +  5)) - ct) <=  683 AND block_level < 1589248 -  4 THEN RETURN true; END IF;
  IF ((SELECT epoch_time_stamp(block_level +  6)) - ct) <=  903 AND block_level < 1589248 -  5 THEN RETURN true; END IF;
  IF ((SELECT epoch_time_stamp(block_level +  7)) - ct) <= 1107 AND block_level < 1589248 -  6 THEN RETURN true; END IF;
  IF ((SELECT epoch_time_stamp(block_level +  8)) - ct) <= 1327 AND block_level < 1589248 -  7 THEN RETURN true; END IF;
  IF ((SELECT epoch_time_stamp(block_level +  9)) - ct) <= 1539 AND block_level < 1589248 -  8 THEN RETURN true; END IF;
  IF ((SELECT epoch_time_stamp(block_level + 10)) - ct) <= 1759 AND block_level < 1589248 -  9 THEN RETURN true; END IF;
  IF ((SELECT epoch_time_stamp(block_level + 11)) - ct) <= 1987 AND block_level < 1589248 - 10 THEN RETURN true; END IF;
  IF ((SELECT epoch_time_stamp(block_level + 12)) - ct) <= 2215 AND block_level < 1589248 - 11 THEN RETURN true; END IF;
  IF ((SELECT epoch_time_stamp(block_level + 13)) - ct) <= 2279 AND block_level < 1589248 - 12 THEN RETURN true; END IF;
  RETURN (select mainnet_block_is_final(block_level + 1));
END;
$$ LANGUAGE PLPGSQL;

DROP FUNCTION IF EXISTS mainnet_block_is_final_pure;
DROP FUNCTION if EXISTS mainnet_block_is_final_pure_direct;


-- The following function is when the optimal time between blocks is 30 seconds.
-- This is meant to be used for edo2net and florencenet.
-- Other testnets might require different values.
-- This function is relevant for the above testnets using Emmy+ consensus algorithm.
CREATE OR REPLACE FUNCTION testnet_block_is_final (block_level int)
RETURNS bool
AS $$
DECLARE
  ct int := (SELECT epoch_time_stamp(block_level));
BEGIN
  IF (select hash from c.chain) = 'NetXz969SFaFn8k' THEN
    RAISE NOTICE 'testnet_block_is_final: granadanet is not supported';
    RETURN false;
  END IF;
  IF ((SELECT level FROM c.block ORDER BY level DESC LIMIT 1) - block_level) <  3 THEN RETURN false; END IF;
  IF ((SELECT epoch_time_stamp(block_level +  3)) - ct) <=  113 THEN RETURN true; END IF;
  IF ((SELECT epoch_time_stamp(block_level +  4)) - ct) <=  227 THEN RETURN true; END IF;
  IF ((SELECT epoch_time_stamp(block_level +  5)) - ct) <=  341 THEN RETURN true; END IF;
  IF ((SELECT epoch_time_stamp(block_level +  6)) - ct) <=  451 THEN RETURN true; END IF;
  IF ((SELECT epoch_time_stamp(block_level +  7)) - ct) <=  553 THEN RETURN true; END IF;
  IF ((SELECT epoch_time_stamp(block_level +  8)) - ct) <=  663 THEN RETURN true; END IF;
  IF ((SELECT epoch_time_stamp(block_level +  9)) - ct) <=  769 THEN RETURN true; END IF;
  IF ((SELECT epoch_time_stamp(block_level + 10)) - ct) <=  879 THEN RETURN true; END IF;
  IF ((SELECT epoch_time_stamp(block_level + 11)) - ct) <=  993 THEN RETURN true; END IF;
  IF ((SELECT epoch_time_stamp(block_level + 12)) - ct) <= 1107 THEN RETURN true; END IF;
  RETURN (select testnet_block_is_final(block_level + 1));
END;
$$ LANGUAGE PLPGSQL;


DROP TABLE IF EXISTS minimal_time_between_blocks;
DROP FUNCTION IF EXISTS minimal_time_between_blocks();


-- Use above functions if you know which one, otherwise the following one is generic, and was tested with edo2net, florencenet (no BA) and mainnet:
CREATE OR REPLACE FUNCTION block_is_final (block_level int)
RETURNS bool
AS $$
DECLARE m int := (select t from minimal_time_between_blocks);
BEGIN
  IF (select is_mainnet())
  THEN
    RETURN (SELECT mainnet_block_is_final (block_level));
  ELSE
    RETURN (SELECT testnet_block_is_final (block_level));
  END IF;
END;
$$ LANGUAGE PLPGSQL;
