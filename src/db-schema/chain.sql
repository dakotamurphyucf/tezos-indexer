-- Open Source License
-- Copyright (c) 2019 Vincent Bernardoff <vb@luminar.eu.org>
-- Copyright (c) 2019-2021 Nomadic Labs <contact@nomadic-labs.com>
--
-- Permission is hereby granted, free of charge, to any person obtaining a
-- copy of this software and associated documentation files (the "Software"),
-- to deal in the Software without restriction, including without limitation
-- the rights to use, copy, modify, merge, publish, distribute, sublicense,
-- and/or sell copies of the Software, and to permit persons to whom the
-- Software is furnished to do so, subject to the following conditions:
--
-- The above copyright notice and this permission notice shall be included
-- in all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
-- THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
-- FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
-- DEALINGS IN THE SOFTWARE.

-----------------------------------------------------------------------------
-----------------------------------------------------------------------------
-- Lines starting with --OPT may be automatically activated
-- Lines ending with --OPT may be automatically deactivated
-----------------------------------------------------------------------------
-----------------------------------------------------------------------------
-- Naming conventions:
-- - for tables:
--   * use singular for names, use plural for column names that are arrays
--   * table 'proposals' is plural because what it contains is plural (it contains 'proposals' on each row)
--   * exceptions: C.addresses (probably should've been named C.address)
-----------------------------------------------------------------------------
-----------------------------------------------------------------------------
-- Foreign keys:
-- They are declared in special comments starting with `--FKEY`, and must start the line.
--  --FKEY name_of_foreign_key ; name_of_table ; set, of, columns ; foreign_table_name(column_name) ; action
-- where action can be CASCADE or SET NULL
-- Refer to existing one for examples.
-----------------------------------------------------------------------------
-----------------------------------------------------------------------------

SELECT 'chain.sql' as file;

-----------------------------------------------------------------------------
-- Some logs about what happens while running the indexer

CREATE TABLE IF NOT EXISTS indexer_log (
   timestamp timestamp DEFAULT CURRENT_TIMESTAMP,
   version text not null default '',
   argv text not null default '',
   action text not null default '',
   primary key (timestamp, version, argv, action)
);
CREATE INDEX IF NOT EXISTS indexer_log_timestamp on indexer_log using btree(timestamp); --1

-----------------------------------------------------------------------------
-- storing the chain id

CREATE TABLE IF NOT EXISTS C.chain ( --L
  hash char(15) primary key
);

CREATE TABLE IF NOT EXISTS C.block_hash ( --NL
  hash char(51) not null
, hash_id int not null
);
CREATE UNIQUE INDEX IF NOT EXISTS block_hash_hash_id on C.block_hash using btree (hash_id); --SEQONLY --1
--PKEY block_hash_pkey; C.block_hash; hash --SEQONLY


-----------------------------------------------------------------------------
-- this table inlines blocks and block headers
-- see lib_base/block_header.mli

CREATE TABLE IF NOT EXISTS C.block ( --NL
  hash_id int not null,
  -- Block hash.
  -- 51 = 32 bytes hashes encoded in b58check + length of prefix "B"
  -- see lib_crypto/base58.ml
  level int not null,
  -- Height of the block, from the genesis block.
  proto smallint not null,
  -- Number of protocol changes since genesis modulo 256.
  predecessor_id int not null,
  -- Hash of the preceding block.
  timestamp timestamp not null,
  -- Timestamp at which the block is claimed to have been created.
  validation_passes smallint not null,
  -- Number of validation passes (also number of lists of operations).
  merkle_root char(53) not null,
  -- see [operations_hash]
  -- Hash of the list of lists (actually root hashes of merkle trees)
  -- of operations included in the block. There is one list of
  -- operations per validation pass.
  -- 53 = 32 bytes hashes encoded in b58 check + "LLo" prefix
  fitness text,
  -- A sequence of sequences of unsigned bytes, ordered by length and
  -- then lexicographically. It represents the claimed fitness of the
  -- chain ending in this block.
  context_hash char(52) not null
  -- Hash of the state of the context after application of this block.
);
--PKEY block_pkey; C.block; hash_id --SEQONLY
--FKEY block_predecessor_id_fkey ; C.block ; hash_id ; C.block_hash(hash_id) ; CASCADE --SEQONLY
--fkey block_predecessor_id_fkey_s ; C.block ; predecessor_id ; C.block(hash_id) ; CASCADE --SEQONLY
CREATE UNIQUE INDEX IF NOT EXISTS block_level    on C.block using btree (level); --SEQONLY --1
CREATE INDEX IF NOT EXISTS block_proto_level     on C.block using btree (proto, level); --SEQONLY --1
-- From v9.4.0, columns "rejected" and "indexing_depth" are no longer created. It's harmless to leave them there, if you have them.
-- From v9.4.0, hash_id=level at all times.

--SELECT setval('C.block_hash_auto_hash_id_seq', coalesce((select level+1 from C.block order by level desc limit 1), 1), false);--SEQONLY


-----------------------------------------------------------------------------
-- implicit operations results (since protocol PtGRANAD) -- since v9.5.0
CREATE TABLE IF NOT EXISTS C.implicit_operations_results ( --NL
  block_hash_id int not null
, operation_kind smallint not null
, consumed_milligas numeric not null
, storage jsonb
, originated_contracts bigint[]
, storage_size numeric
, paid_storage_size_diff numeric
, allocated_destination_contract bool
, strings text[]
, id int not null
, originated_tx_rollup_id bigint  -- since v9.9.3
, sc_rollup_address_id bigint -- since v9.9.3
, sc_rollup_size numeric -- since v9.8.2
, sc_rollup_inbox_after jsonb -- since v9.9
-- balance updates are detailed in C.balance_updates
);
CREATE UNIQUE INDEX IF NOT EXISTS implicit_operations_results_block_id on C.implicit_operations_results using btree (block_hash_id, id); --SEQONLY --1
CREATE INDEX IF NOT EXISTS implicit_operations_results_block on C.implicit_operations_results using btree (block_hash_id); --SEQONLY --1
CREATE INDEX IF NOT EXISTS implicit_operations_results_opkind on C.implicit_operations_results using btree (operation_kind); --SEQONLY --1



-----------------------------------------------------------------------------
-- operations seen from block-level, therefore non-protocol-specific information

CREATE TABLE IF NOT EXISTS C.operation ( --NL
  -- Note: an operation may point to a rejected block only if the
  -- operation itself was deleted from the chain.
  -- If the operation was included in a rejected block but then
  -- reinjected into another block, then this table contains the
  -- latest block_hash associated to that operation.
  -- Hypothesis: the latest write is always right.
  hash char(51) not null, -- operation hash
  block_hash_id int not null, -- char(51) not null, -- block hash
  hash_id bigint not null
);
--FKEY operation_block_hash_id_fkey; C.operation; block_hash_id; C.block(hash_id) ; CASCADE --SEQONLY
--PKEY operation_pkey; C.operation; hash_id --SEQONLY

CREATE INDEX IF NOT EXISTS operation_block on C.operation using btree (block_hash_id); --SEQONLY --1
CREATE INDEX IF NOT EXISTS operation_hash on C.operation using btree (hash); --SEQONLY --1
CREATE INDEX IF NOT EXISTS operation_hash_id on C.operation using btree (hash_id); --SEQONLY --1

CREATE OR REPLACE FUNCTION operation_hash(id bigint) returns char as $$ select hash from C.operation where hash_id = id $$ language sql stable;

CREATE OR REPLACE FUNCTION operation_hash_id(h char) returns bigint as $$ select hash_id from C.operation where hash = h $$ language sql stable;

-----------------------------------------------------------------------------
-- Index of protocol-specific contents of an operation
-- An "operation" at the "shell" level is a "set of operations" at the "protocol" level.
-- In the following table, "hash_id" refers to an operation at the shell level.
-- At the protocol level, a shell-level operation is a list of operations (the word "operation" has different meanings).
-- Inside protocol-level operations, there can be some additional "internal operations".
-- "Internal operations" (a.k.a. "internal manager operation") are operations that are at manager-operation-level.
-- An "internal operation", so far, for protocols 1 to 11, are only manager operations (inside manager operations).
-- There are differences between "manager operations" and "internal manager operations".
-- For instance, internal ones don't have data in manager_numbers, but are the only ones that have a "nonce" (which are integers).

CREATE TABLE IF NOT EXISTS C.operation_alpha ( --NL
    block_hash_id int not null
  -- block hash id
  , hash_id bigint not null
  -- operation hash id
  , id smallint not null
  -- index of op in contents_list
  , operation_kind smallint not null
  -- 0: Endorsement
  -- 1: Seed_nonce_revelation
  -- 2: double_endorsement_evidence
  -- 3: Double_baking_evidence
  -- 4: Activate_account
  -- 5: Proposals
  -- 6: Ballot
  -- 7: Manager_operation { operation = Reveal _ ; _ }
  -- 8: Manager_operation { operation = Transaction _ ; _ }
  -- 9: Manager_operation { operation = Origination _ ; _ }
  -- 10: Manager_operation { operation = Delegation _ ; _ }
  -- 13: Manager_operation { operation = Register_global_constant _ ; _ }
  -- 14: Manager_operation { operation = Set_deposits_limit _ ; _ }
  -- 17: Manager_operation { operation = Tx_rollup_origination _ ; _ }
  -- 18: Manager_operation { operation = Sc_rollup_originate _ ; _ }
  -- 19: Manager_operation { operation = Sc_rollup_add_messages _ ; _ }
  -- 20: Manager_operation { operation =  }
  -- 21: Manager_operation { operation =  }
  -- 11: Endorsement_with_slot
  -- 12: Failing_noop
  -- 15: Preendorsement
  -- 16: Double_preendorsement_evidence
  , internal smallint not null
  -- block hash
  , autoid bigint not null -- counter id
);
--PKEY operation_alpha_pkey; C.operation_alpha; hash_id, id, internal, block_hash_id --SEQONLY
--FKEY operation_alpha_hash_fkey; C.operation_alpha; hash_id; C.operation(hash_id) ; CASCADE --SEQONLY
--FKEY operation_alpha_block_hash_fkey; C.operation_alpha; block_hash_id; C.block(hash_id) ; CASCADE --SEQONLY
CREATE INDEX IF NOT EXISTS operation_alpha_kind on C.operation_alpha using btree (operation_kind); --SEQONLY --1
CREATE INDEX IF NOT EXISTS operation_alpha_hash on C.operation_alpha using btree (hash_id); --SEQONLY --1
CREATE INDEX IF NOT EXISTS operation_alpha_block_hash on C.operation_alpha using btree (block_hash_id); --SEQONLY --1
CREATE UNIQUE INDEX IF NOT EXISTS operation_alpha_autoid on C.operation_alpha using btree (autoid); --1

CREATE OR REPLACE FUNCTION operation_kind (k smallint)
RETURNS	char
AS
$$
SELECT
coalesce((select 'endorsement' where k = 0),
coalesce((select 'seed nonce revelation' where k = 1),
coalesce((select 'double endorsement evidence' where k = 2),
coalesce((select 'double baking evidence' where k = 3),
coalesce((select 'activate account' where k = 4),
coalesce((select 'proposals' where k = 5),
coalesce((select 'ballot' where k = 6),
coalesce((select 'reveal' where k = 7),
coalesce((select 'transaction' where k = 8),
coalesce((select 'origination' where k = 9),
coalesce((select 'delegation' where k = 10),
coalesce((select 'endorsement_with_slot' where k = 11),
coalesce((select 'register_global_constant' where k = 13),
coalesce((select 'set_deposits_limit' where k = 14),
coalesce((select 'preendorsement' where k = 15),
coalesce((select 'double_preendorsement_evidence' where k = 16),
coalesce((select 'tx_rollup_origination' where k = 17),
coalesce((select 'sc_rollup_originate' where k = 18),
coalesce((select 'sc_rollup_add_messages' where k = 19),
coalesce((select 'tx_rollup_submit_batch' where k = 20),
coalesce((select 'tx_rollup_commit' where k = 21),
         (select 'failing_noop' where k = 12))))))))))))))))))))));
$$ LANGUAGE SQL STABLE;

CREATE OR REPLACE FUNCTION operation_kind (k int)
RETURNS char
AS
$$
SELECT operation_kind(k::smallint)
$$ LANGUAGE SQL STABLE;


CREATE OR REPLACE FUNCTION operation_alpha_autoid(ophid bigint, opid smallint, i smallint, bhid bigint)
RETURNS bigint
AS $$
SELECT autoid
FROM C.operation_alpha
WHERE (hash_id, id, internal, block_hash_id) = (ophid, opid, i, bhid);
$$ LANGUAGE SQL STABLE;

DROP FUNCTION IF EXISTS operation_hash_alpha; --change of signature in v9.7.7
CREATE OR REPLACE FUNCTION operation_hash_alpha(opaid bigint)
RETURNS char as $$
select o.hash from c.operation o, c.operation_alpha a where a.autoid = opaid and a.hash_id = o.hash_id
$$ LANGUAGE SQL STABLE;

CREATE OR REPLACE FUNCTION operation_hash_id_alpha(opaid bigint)
returns bigint as $$ select hash_id from C.operation_alpha where autoid = opaid $$ language sql stable;

CREATE OR REPLACE FUNCTION operation_id_alpha(opaid bigint)
returns smallint as $$ select id from C.operation_alpha where autoid = opaid $$ language sql stable;

CREATE OR REPLACE FUNCTION operation_alpha_ids(h char)
returns table (id bigint) as $$ select autoid from C.operation_alpha where hash_id = operation_hash_id(h) $$ language sql stable;

CREATE OR REPLACE FUNCTION operation_id (h char, i smallint, inter smallint)
returns bigint
as $$ select autoid from c.operation_alpha where hash_id = operation_hash_id(h) and id = i and internal = inter $$ language SQL stable;
CREATE OR REPLACE FUNCTION operation_id (h char, i int, inter int)
returns bigint
as $$ select autoid from c.operation_alpha where hash_id = operation_hash_id(h) and id = i::smallint and internal = inter::smallint $$ language SQL stable;


CREATE OR REPLACE FUNCTION block_hash(id int) returns char as $$ select hash from C.block_hash where hash_id = id $$ language sql stable;
CREATE OR REPLACE FUNCTION op_shift() returns bigint as $$ select 10000000::bigint $$ language sql immutable;
CREATE OR REPLACE FUNCTION block_hash_id_from_opid(opid bigint) returns int as $$ select (opid / op_shift())::int where opid > 0 $$ language sql immutable;
CREATE OR REPLACE FUNCTION block_hash(opid bigint) returns char as $$ select hash from C.block_hash where hash_id = block_hash_id_from_opid(opid) $$ language sql stable;
CREATE OR REPLACE FUNCTION block_hash_id(h char) returns int as $$ select hash_id from C.block_hash where hash = h $$ language sql stable;
CREATE OR REPLACE FUNCTION block_level_of_block_hash_id(bhid int) returns int as $$ select bhid $$ language sql stable;
CREATE OR REPLACE FUNCTION block_level_of_opaid(opaid bigint) returns int as $$ select block_hash_id_from_opid(opaid) $$ language sql stable;
CREATE OR REPLACE FUNCTION block_level_of_operation_hash_id(ophid bigint) returns int as $$ select block_hash_id_from_opid(ophid) $$ language sql stable;

-----------------------------------------------------------------------------
-- Convenience table to rapidly get a list of operations linked to an address

CREATE TABLE IF NOT EXISTS C.operation_sender_and_receiver ( --L
  operation_id bigint not null,
  sender_id bigint not null,
  receiver_id bigint
);
--PKEY operation_sender_and_receiver_pkey; C.operation_sender_and_receiver; operation_id --SEQONLY
--FKEY operation_sender_and_receiver_operation_id_fkey; C.operation_sender_and_receiver; operation_id; C.operation_alpha(autoid) ; CASCADE --SEQONLY
--FKEY operation_sender_and_receiver_sender_id_fkey; C.operation_sender_and_receiver; sender_id; C.addresses(address_id) ; CASCADE
--FKEY operation_sender_and_receiver_receiver_id_fkey; C.operation_sender_and_receiver; receiver_id; C.addresses(address_id) ; CASCADE

-- since v9.9.3, the following 2 indexes replace indexes on solely sender_id and receiver_id
CREATE INDEX IF NOT EXISTS operation_sender_and_receiver_sender_opaid on C.operation_sender_and_receiver using btree (sender_id, operation_id); --SEQONLY --1
CREATE INDEX IF NOT EXISTS operation_sender_and_receiver_receiver_opaid on C.operation_sender_and_receiver using btree (receiver_id, operation_id); --SEQONLY --1

-----------------------------------------------------------------------------
-- Protocol amendment proposals

CREATE TABLE IF NOT EXISTS C.proposals ( --NL
  proposal char(51) not null,
  proposal_id bigint not null
  -- about proposal_id: the important factor is to have a unique
  -- value. That value could be smaller and probably using a smallint
  -- would be enough to represent all proposals happening on tezos for
  -- many years to come. However here we use a bigint, to "simply" use
  -- the first operation's ophid that needs to access `proposal_id`.
  -- The first operation that needs that access will write the
  -- proposal's hash value into the `proposal` column, and give its
  -- `ophid` as `proposal_id`. Further operations will attempt to do
  -- the same, and will fail the writing (since `proposal` is a pkey)
  -- but will be able to access `proposal_id`.
  -- All that is to avoid using Postgresql's SERIAL because those
  -- generate values that cannot be accessed within an SQL transaction.
);
--PKEY proposals_pkey; C.proposals; proposal
CREATE UNIQUE INDEX IF NOT EXISTS proposals_proposal_id on C.proposals using btree (proposal_id); --1

-- pre-filling c.proposals for multicore mode
-- note that the proposal_id is (generally) NOT the protocol number
insert into c.proposals (proposal, proposal_id) values --MULTICORE
('PsDELPH1Kxsxt8f9eWbxQeRxkjfbxoqM52jvs5Y5fBxWWh4ifpo', 1), --MULTICORE
('PtEdoTezd3RHSC31mpxxo1npxFjoWWcFgQtxapi51Z8TLu6v6Uq', 2), --MULTICORE
('PsCARTHAGazKbHtnKfLzQg3kms52kSRpgnDY982a9oYsSXRLQEb', 3), --MULTICORE
('PtCarthavAMoXqbjBPVgDCRd5LgT7qqKWUPXnYii3xCaHRBMfHH', 4), --MULTICORE
('PsBABY5HQTSkA4297zNHfsZNKtxULfL18y95qb3m53QJiXGmrbU', 5), --MULTICORE
('PtdRxBHvc91c2ea2evV6wkoqnzW7TadTg9aqS9jAn2GbcPGtumD', 6), --MULTICORE
('Pt24m4xiPbLDhVgVfABUjirbmda3yohdN82Sp9FeuAXJ4eV9otd', 7), --MULTICORE
('PsBABY5nk4JhdEv1N1pZbt6m6ccB9BfNqa23iKZcHBh23jmRS9f', 8), --MULTICORE
('Psd1ynUBhMZAeajwcZJAeq5NrxorM6UCU4GJqxZ7Bx2e9vUWB6z', 9), --MULTICORE
('PtGRANADsDU8R9daYKAgWnQYAJ64omN1o3KMGVCykShA97vQbvV', 10) --MULTICORE
on conflict do nothing; --MULTICORE


CREATE OR REPLACE FUNCTION proposal(id bigint) returns char as $$ select proposal from C.proposals where proposal_id = id $$ language sql stable;

CREATE OR REPLACE FUNCTION proposal_id(p char) returns bigint as $$ select proposal_id from C.proposals where proposal = p $$ language sql stable;

CREATE TABLE IF NOT EXISTS C.proposal ( --L
    operation_id bigint not null
  , source_id bigint not null
  , period int not null
  , proposal_id bigint not null
);
--PKEY proposal_pkey; C.proposal; operation_id, proposal_id
--FKEY proposal_operation_id_fkey; C.proposal; operation_id; C.operation_alpha(autoid); CASCADE --SEQONLY
--FKEY proposal_source_fkey; C.proposal; source_id; C.addresses(address_id); CASCADE --SEQONLY
--FKEY proposal_proposal_id_fkey; C.proposal; proposal_id; C.proposals(proposal_id); CASCADE --SEQONLY
CREATE INDEX IF NOT EXISTS proposal_operation on C.proposal using btree (operation_id); --1
CREATE INDEX IF NOT EXISTS proposal_source on C.proposal using btree (source_id); --SEQONLY --1
CREATE INDEX IF NOT EXISTS proposal_proposal on C.proposal using btree (proposal_id); --1
CREATE INDEX IF NOT EXISTS proposal_period on C.proposal using btree (period); --SEQONLY --1

-----------------------------------------------------------------------------
-- Ballots for proposal amendment proposals
DO $$
BEGIN
  IF NOT EXISTS (SELECT 1 FROM pg_type WHERE typname = 'one_ballot') THEN
    CREATE TYPE one_ballot AS ENUM ('nay', 'yay', 'pass');
  END IF;
END
$$;

CREATE TABLE IF NOT EXISTS C.ballot ( --L
    operation_id bigint not null
  , source_id bigint not null
  , period int not null
  , proposal_id bigint not null
  , ballot one_ballot not null
);
--PKEY ballot_pkey; C.ballot; operation_id --SEQONLY
--FKEY ballot_operation_id_fkey; C.ballot; operation_id; C.operation_alpha(autoid); CASCADE --SEQONLY
--FKEY ballot_proposal_id_fkey; C.ballot; proposal_id; C.proposals(proposal_id); CASCADE --SEQONLY
CREATE INDEX IF NOT EXISTS ballot_source on C.ballot using btree (source_id); --SEQONLY --1


-----------------------------------------------------------------------------
-- Double endorsement evidence

CREATE TABLE IF NOT EXISTS C.double_endorsement_evidence ( --L
    operation_id bigint not null
  , op1 jsonb
  , op2 jsonb
  , op1_ json
  , op2_ json
);
--PKEY double_endorsement_evidence_pkey; C.double_endorsement_evidence; operation_id --SEQONLY
--FKEY double_endorsement_evidence_operation_id_fkey; C.double_endorsement_evidence; operation_id; C.operation_alpha(autoid); CASCADE --SEQONLY

-----------------------------------------------------------------------------
-- Double preendorsement evidence

CREATE TABLE IF NOT EXISTS C.double_preendorsement_evidence ( --L
    operation_id bigint not null
  , op1 jsonb
  , op2 jsonb
  , op1_ json
  , op2_ json
);
--PKEY double_preendorsement_evidence_pkey; C.double_preendorsement_evidence; operation_id --SEQONLY
--FKEY double_preendorsement_evidence_operation_id_fkey; C.double_preendorsement_evidence; operation_id; C.operation_alpha(autoid); CASCADE --SEQONLY


-----------------------------------------------------------------------------
-- Double baking evidence

CREATE TABLE IF NOT EXISTS C.double_baking_evidence ( --L
    operation_id bigint not null
  , bh1 jsonb -- block header 1
  , bh2 jsonb -- block header 2
  , bh1_ json
  , bh2_ json
);
--PKEY double_baking_evidence_pkey; C.double_baking_evidence; operation_id --SEQONLY
--FKEY double_baking_evidence_operation_id_fkey; C.double_baking_evidence;operation_id; C.operation_alpha(autoid); CASCADE --SEQONLY


-----------------------------------------------------------------------------
-- Common data for manager operations

CREATE TABLE IF NOT EXISTS C.manager_numbers ( --NL
    operation_id bigint not null
  , counter numeric not null
  -- counter
  , gas_limit numeric not null
  -- gas limit
  , storage_limit numeric not null
  -- storage limit
);
--PKEY manager_numbers_pkey; C.manager_numbers; operation_id --SEQONLY
--FKEY manager_numbers_operation_id_fkey; C.manager_numbers; operation_id; C.operation_alpha(autoid); CASCADE --SEQONLY
-- Not sure the following indexes are relevant:
--OPT CREATE INDEX IF NOT EXISTS manager_numbers_counter on C.manager_numbers using btree (counter); --SEQONLY
--OPT CREATE INDEX IF NOT EXISTS manager_numbers_gas_limit on C.manager_numbers using btree (gas_limit); --SEQONLY
--OPT CREATE INDEX IF NOT EXISTS manager_numbers_storage_limit on C.manager_numbers using btree (storage_limit); --SEQONLY


-----------------------------------------------------------------------------
-- Account Activations

CREATE TABLE IF NOT EXISTS C.activation ( --L
   operation_id bigint not null
 , pkh_id bigint not null
 , activation_code text not null
);
--PKEY activation_pkey; C.activation; operation_id --SEQONLY
--FKEY activation_operation_id_fkey; C.activation; operation_id; C.operation_alpha(autoid) ; CASCADE --SEQONLY
--FKEY activation_pkh_fkey; C.activation; pkh_id; C.addresses(address_id) ; CASCADE --SEQONLY


-----------------------------------------------------------------------------
-- Endorsements

CREATE TABLE IF NOT EXISTS C.endorsement ( --L
   operation_id bigint not null
 , level int
 , delegate_id bigint
 , slots smallint[]
 , round int
 , block_payload_hash char(52)
 , endorsement_power int -- nullable because not present until proto 12
);
--PKEY endorsement_pkey; C.endorsement; operation_id --SEQONLY
--FKEY endorsement_operation_id_fkey; C.endorsement; operation_id; C.operation_alpha(autoid) ; CASCADE --SEQONLY
--FKEY endorsement_delegate_fkey; C.endorsement; delegate_id; C.addresses(address_id) ; CASCADE --SEQONLY
CREATE INDEX IF NOT EXISTS endorsement_delegate_id on C.endorsement using btree (delegate_id); --SEQONLY --1
CREATE INDEX IF NOT EXISTS endorsement_level on C.endorsement using btree (level); --SEQONLY --1


-----------------------------------------------------------------------------
-- Preendorsements

CREATE TABLE IF NOT EXISTS C.preendorsement ( --L
   operation_id bigint not null
 , level int
 , delegate_id bigint
 , slots smallint[]
 , round int
 , block_payload_hash char(52)
 , preendorsement_power int -- nullable because not present until proto 12
);
--PKEY preendorsement_pkey; C.preendorsement; operation_id --SEQONLY
--FKEY preendorsement_operation_id_fkey; C.preendorsement; operation_id; C.operation_alpha(autoid) ; CASCADE --SEQONLY
--FKEY preendorsement_delegate_fkey; C.preendorsement; delegate_id; C.addresses(address_id) ; CASCADE --SEQONLY
CREATE INDEX IF NOT EXISTS preendorsement_delegate_id on C.preendorsement using btree (delegate_id); --SEQONLY --1
CREATE INDEX IF NOT EXISTS preendorsement_level on C.preendorsement using btree (level); --SEQONLY --1


-----------------------------------------------------------------------------
-- Seed nonce revelation

CREATE TABLE IF NOT EXISTS C.seed_nonce_revelation ( --L
    operation_id bigint not null
  -- index of the operation in the block's list of operations
 , level int not null
 , nonce char(66) not null
);
--PKEY seed_nonce_revelation_pkey; C.seed_nonce_revelation; operation_id --SEQONLY
--FKEY seed_nonce_revelation_operation_id_fkey; C.seed_nonce_revelation; operation_id; C.operation_alpha(autoid); CASCADE --SEQONLY


-----------------------------------------------------------------------------
-- Blocks at alpha level.
-- "level_position = cycle * blocks_per_cycle + cycle_position"

CREATE TABLE IF NOT EXISTS C.block_alpha ( --L
  hash_id int not null
  -- block hash id
  , baker_id bigint not null
  -- pkh of baker
  , level_position int not null
  /* Verbatim from lib_protocol/level_repr:
     The level of the block relative to the block that
     starts protocol alpha. This is specific to the
     protocol alpha. Other protocols might or might not
     include a similar notion.
  */
  , cycle int not null
  -- cycle
  , cycle_position int not null
  /* Verbatim from lib_protocol/level_repr:
     The current level of the block relative to the first
     block of the current cycle.
  */
  , voting_period jsonb not null
  /* increasing integer.
     from proto_alpha/level_repr:
     voting_period = level_position / blocks_per_voting_period */
  , voting_period_position int not null
  -- voting_period_position = remainder(level_position / blocks_per_voting_period)
  , voting_period_kind smallint not null
  /* Proposal = 0
     Testing_vote = 1
     Testing = 2
     Promotion_vote = 3
     Adoption = 4
   */
  , consumed_milligas numeric not null
  /* total milligas consumed by block. Arbitrary-precision integer. */
);
--PKEY block_alpha_pkey; C.block_alpha; hash_id --SEQONLY
--FKEY block_alpha_hash_fkey; C.block_alpha; hash_id; C.block(hash_id); CASCADE --SEQONLY
--FKEY block_alpha_baker_fkey; C.block_alpha; baker_id; C.addresses(address_id); CASCADE --SEQONLY
CREATE INDEX IF NOT EXISTS block_alpha_baker on C.block_alpha using btree (baker_id); --SEQONLY --1
CREATE INDEX IF NOT EXISTS block_alpha_level_position on C.block_alpha using btree (level_position); --SEQONLY --1
CREATE INDEX IF NOT EXISTS block_alpha_cycle on C.block_alpha using btree (cycle); --SEQONLY --1
CREATE INDEX IF NOT EXISTS block_alpha_cycle_position on C.block_alpha using btree (cycle_position); --SEQONLY --1
-- CREATE INDEX IF NOT EXISTS block_alpha_hash on C.block_alpha using btree (hash_id); --useless if hash_id is pkey

-----------------------------------------------------------------------------
-- Deactivated accounts

CREATE TABLE IF NOT EXISTS C.deactivated ( --L
  pkh_id bigint not null,
  -- pkh of the deactivated account(tz1...)
  block_hash_id int not null
  -- block hash at which deactivation occured
);
--PKEY deactivated_pkey; C.deactivated; pkh_id, block_hash_id  --SEQONLY
--FKEY deactivated_pkh_fkey; C.deactivated; pkh_id; C.addresses(address_id); CASCADE --SEQONLY
--FKEY deactivated_block_hash_fkey; C.deactivated; block_hash_id; C.block(hash_id); CASCADE --SEQONLY
CREATE INDEX IF NOT EXISTS deactivated_pkh on C.deactivated using btree (pkh_id); --SEQONLY --1
CREATE INDEX IF NOT EXISTS deactivated_block_hash on C.deactivated using btree (block_hash_id); --SEQONLY --1
--CREATE INDEX IF NOT EXISTS deactivated_autoid on C.deactivated using btree (autoid); --OPT --SEQONLY --FIXME: is it useful?


-----------------------------------------------------------------------------
-- Table of contract balance by block level: each time a contract has its balance updated, we write it here

CREATE TABLE IF NOT EXISTS C.contract_balance ( --L
  address_id bigint not null,
  block_hash_id int not null,
  balance bigint -- make it nullable so that it can be filled asynchronously
  -- N.B. it would be bad to have "address_id" as a primary key,
  -- because if you update a contract's balance using a
  -- rejected block(uncle block) and then the new balance is not updated
  -- once the rejected block is discovered,
  -- you end up with wrong information
);
--PKEY contract_balance_pkey; C.contract_balance; address_id, block_hash_id  --SEQONLY
--FKEY contract_balance_address_fkey; C.contract_balance; address_id; C.addresses(address_id); CASCADE --SEQONLY
--FKEY contract_balance_block_hash_fkey; C.contract_balance; block_hash_id; C.block(hash_id); CASCADE --SEQONLY
CREATE INDEX IF NOT EXISTS contract_balance_address on C.contract_balance using btree (address_id); --SEQONLY --1
CREATE INDEX IF NOT EXISTS contract_balance_balance on C.contract_balance using btree (balance); --SEQONLY --1
CREATE INDEX IF NOT EXISTS contract_balance_address_block_level on C.contract_balance using btree (address_id, block_hash_id desc); --SEQONLY --1
CREATE INDEX IF NOT EXISTS contract_balance_block_hash on C.contract_balance using btree (block_hash_id); --SEQONLY --1

-- the following index is for insertion performance
CREATE INDEX IF NOT EXISTS contract_balance_balance_bhid on C.contract_balance using btree (balance, block_hash_id); --SEQONLY --1

-----------------------------------------------------------------------------
-- Table of contract balance by block level: each time a contract has its balance updated, we write it here

CREATE TABLE IF NOT EXISTS C.contract_script ( -- since v9.1.0 --L
  address_id bigint primary key
, script jsonb -- make it nullable so that it can be filled asynchronously
, block_hash_id int -- if null then value of `script` is unreliable and should be updated, if not null, then it's origin of the script
, strings text[] -- since v9.3.0
, uri int[]
, contracts bigint[]
, script_ json
, missing_script smallint -- not null means the indexer failed to get the script -- since v9.5.0
);
--FKEY contract_script_address_fkey; C.contract_script; address_id; C.addresses(address_id); CASCADE --SEQONLY
--FKEY contract_script_block_hash_id_fkey; C.contract_script; block_hash_id; C.block_hash(hash_id); SET NULL --SEQONLY
CREATE INDEX IF NOT EXISTS contract_script_address on C.contract_script using btree (address_id); --SEQONLY --1
CREATE INDEX IF NOT EXISTS contract_script_block_hash on C.contract_script using btree (block_hash_id); --SEQONLY --1
CREATE INDEX IF NOT EXISTS contract_script_missing_script on C.contract_script using btree (missing_script); --SEQONLY --1
--CREATE INDEX IF NOT EXISTS contract_script_strings on C.contract_script using GIN (strings); --SEQONLY

-- the following index is for speeding up insertions
CREATE INDEX IF NOT EXISTS contract_script_address_id_script on C.contract_script using btree (address_id, (script is null)); --SEQONLY --1
-- the following index is for speeding up insertions
CREATE INDEX IF NOT EXISTS contract_script_address_id_script_missing_script on C.contract_script using btree (address_id, (script is null), (script_ is null), (missing_script is null)); --SEQONLY --1

-- BEGIN keep up to date with conversion_from_multicore.sql
CREATE INDEX IF NOT EXISTS contract_script_uri on C.contract_script using GIN (uri); --SEQONLY --1
CREATE INDEX IF NOT EXISTS contract_script_contracts on C.contract_script using GIN (contracts); --SEQONLY --1
-- END keep up to date with conversion_from_multicore.sql

INSERT INTO C.contract_script (address_id) select address_id from C.addresses where address < 't' on conflict do nothing; --SEQONLY



-----------------------------------------------------------------------------
-- Transactions -- manop

CREATE TABLE IF NOT EXISTS C.tx ( --L
  operation_id bigint not null
  -- operation id from operation_alpha
, source_id bigint not null
  -- source address
, destination_id bigint not null
  -- dest address
, fee bigint not null
  -- fees
, amount bigint not null
  -- amount
, originated_contracts bigint[]
  -- since v9.5.0
, parameters jsonb -- jsonb since v9.3 -- invalid Unicode sequences are replaced by an error message
  -- optional parameters to contract in json-encoded Micheline
, storage jsonb
  -- optional parameter for storage update
, consumed_milligas numeric
  -- consumed milligas
, storage_size numeric
  -- storage size
, paid_storage_size_diff numeric
  -- paid storage size diff
, entrypoint text
  -- entrypoint
, nonce int -- non null for internal operations
, status smallint not null default 0 -- 0: applied, 1: backtracked, 2: failed, 3: skipped
, error_trace jsonb
, strings text[] -- since v9.3.0
, uri int[]
, contracts bigint[]
, parameters_ json
, storage_ json
);
--PKEY tx_pkey; C.tx; operation_id --SEQONLY
--FKEY tx_operation_id_fkey; C.tx; operation_id; C.operation_alpha(autoid); CASCADE --SEQONLY
--FKEY tx_source_fkey; C.tx; source_id; C.addresses(address_id); CASCADE --SEQONLY
--FKEY tx_destination_fkey; C.tx; destination_id; C.addresses(address_id); CASCADE --SEQONLY
CREATE INDEX IF NOT EXISTS tx_source on C.tx using btree (source_id); --SEQONLY --1
CREATE INDEX IF NOT EXISTS tx_destination on C.tx using btree (destination_id); --SEQONLY --1
--CREATE INDEX IF NOT EXISTS tx_strings on C.tx using GIN (strings); --SEQONLY

-- BEGIN keep up to date with conversion_from_multicore.sql
CREATE INDEX IF NOT EXISTS tx_uri on C.tx using GIN (uri); --SEQONLY --1
CREATE INDEX IF NOT EXISTS tx_contracts on C.tx using GIN (contracts); --SEQONLY --1
-- END keep up to date with conversion_from_multicore.sql

-----------------------------------------------------------------------------
-- Origination table -- manop

CREATE TABLE IF NOT EXISTS C.origination ( --L
  operation_id bigint not null
  -- operation id from operation_alpha
, source_id bigint not null
  -- source of origination op
, k_id bigint
  -- address of originated contract
, consumed_milligas numeric
  -- consumed milligas
, storage_size numeric
  -- storage size
, paid_storage_size_diff numeric
  -- paid storage size diff
, fee bigint not null
  -- fees
, nonce int -- non null for internal operations
, preorigination_id bigint -- optional for protos 1 to 8
, delegate_id bigint -- optional for protos 1,2,3,4,5,6,7,8
, credit bigint not null
, status smallint not null default 0 -- 0: applied, 1: backtracked, 2: failed, 3: skipped
, error_trace jsonb
, error_trace_ json
-- for script, cf. C.contract_script
-- for manager, cf. C.manager -- protos 1 to 4 included only
);
--PKEY origination_pkey; C.origination; operation_id --SEQONLY
--FKEY origination_operation_id_fkey; C.origination; operation_id; C.operation_alpha(autoid); CASCADE --SEQONLY
--FKEY origination_operation_source_fkey; C.origination; source_id; C.addresses(address_id); CASCADE --SEQONLY
--FKEY origination_k_fkey; C.origination; k_id; C.addresses(address_id); CASCADE --SEQONLY
CREATE INDEX IF NOT EXISTS origination_source         on C.origination using btree (source_id); --SEQONLY --1
CREATE INDEX IF NOT EXISTS origination_k              on C.origination using btree (k_id); --SEQONLY --1
CREATE INDEX IF NOT EXISTS origination_preorigination on C.origination using btree (preorigination_id); --SEQONLY --1
CREATE INDEX IF NOT EXISTS origination_delegate       on C.origination using btree (delegate_id); --SEQONLY --1


-----------------------------------------------------------------------------
-- Manager table for old protocols (1 to 4)
-- This would be part of C.origination if it was not dropped by protocol 5 and replaced by a non-null script (which was optional before protocol 5).
-- You might want to choose not to fill this table at all. In that case, modify I.origination in chain_functions.sql to simply remove the insertion.

CREATE TABLE IF NOT EXISTS C.manager ( --L
  operation_id bigint not null
, manager_id bigint not null
);
--PKEY manager_pkey; C.manager; operation_id --SEQONLY
--FKEY manager_operation_id_fkey; C.manager; operation_id; C.operation_alpha(autoid); CASCADE --SEQONLY
--FKEY manager_manager_id_fkey; C.manager; manager_id; C.addresses(address_id); CASCADE --SEQONLY


-----------------------------------------------------------------------------
-- Delegation -- manop

CREATE TABLE IF NOT EXISTS C.delegation ( --L
  operation_id bigint not null
  -- operation id from operation_alpha
  , source_id bigint not null
  -- source of the delegation op
  , pkh_id bigint
  -- optional delegate
  , consumed_milligas numeric
  -- consumed milligas
  , fee bigint not null
  -- fees
  , nonce int -- non null for internal operations
  , status smallint not null default 0 -- 0: applied, 1: backtracked, 2: failed, 3: skipped
  , error_trace jsonb
  , error_trace_ json
);
--PKEY delegation_pkey; C.delegation; operation_id --SEQONLY
--FKEY delegation_operation_id_fkey; C.delegation; operation_id; C.operation_alpha(autoid); CASCADE --SEQONLY
--FKEY delegation_source_fkey; C.delegation; source_id; C.addresses(address_id); CASCADE --SEQONLY
--FKEY delegation_pkh_fkey; C.delegation; pkh_id; C.addresses(address_id); CASCADE --SEQONLY
CREATE INDEX IF NOT EXISTS delegation_source         on C.delegation using btree (source_id); --SEQONLY --1
CREATE INDEX IF NOT EXISTS delegation_pkh            on C.delegation using btree (pkh_id); --SEQONLY --1
CREATE INDEX IF NOT EXISTS delegation_operation_hash on C.delegation using btree (operation_id); --SEQONLY --1


-----------------------------------------------------------------------------
-- Reveals -- manop

CREATE TABLE IF NOT EXISTS C.reveal ( --L
    operation_id bigint not null
  -- operation id from operation_alpha
  , source_id bigint not null
  -- source
  , pk varchar(55) not null
  -- revealed pk
  , consumed_milligas numeric
  -- consumed milligas
  , fee bigint not null
  -- fees
  , nonce int -- non null for internal operations
  , status smallint not null default 0 -- 0: applied, 1: backtracked, 2: failed, 3: skipped
  , error_trace jsonb
  , error_trace_ json
);
--PKEY reveal_pkey; C.reveal; operation_id --SEQONLY
--FKEY reveal_operation_hash_op_id_fkey; C.reveal; operation_id; C.operation_alpha(autoid); CASCADE --SEQONLY
--FKEY reveal_source_fkey; C.reveal; source_id; C.addresses(address_id); CASCADE --SEQONLY
CREATE INDEX IF NOT EXISTS reveal_source         on C.reveal using btree (source_id); --SEQONLY --1
CREATE INDEX IF NOT EXISTS reveal_pkh            on C.reveal using btree (pk); --SEQONLY --1


-----------------------------------------------------------------------------
-- global constants -- since v9.7.0
CREATE TABLE IF NOT EXISTS C.global_constants ( --NL
  global_address char(54) not null
, "value" jsonb not null
, global_address_id bigint not null
, size int not null
-- , strings text[]
);
CREATE UNIQUE INDEX IF NOT EXISTS global_constants_address on C.global_constants using btree (global_address); --SEQONLY --1
-- CREATE UNIQUE INDEX IF NOT EXISTS global_constants_value on C.global_constants using btree ("value"); -- this index crashes on a value that is too large to fit
DROP INDEX IF EXISTS c.global_constants_value; --since v9.7.b and v9.8.3
CREATE UNIQUE INDEX IF NOT EXISTS global_constants_address_id on C.global_constants using btree (global_address_id); --SEQONLY --1

CREATE OR REPLACE FUNCTION global_address_id(ga char)
RETURNS bigint
AS $$
SELECT global_address_id FROM C.global_constants WHERE global_address = ga;
$$ LANGUAGE SQL STABLE;

-----------------------------------------------------------------------------
-- Register_Global_Constant -- manop -- since v9.7.0

CREATE TABLE IF NOT EXISTS C.register_global_constant ( -- introduced by proto 11 --L
  operation_id bigint not null
  -- operation id from operation_alpha
  , source_id bigint not null
  -- source of the register_global_constant op
  , global_address_id bigint -- this is equal to operation_id if all goes well, otherwise it's equal to another operation's operation_id: the operation that is indexed first will give its operation_id
  -- id of hash of the "value"
  , consumed_milligas numeric
  -- consumed milligas
  , fee bigint not null
  -- fees
  , nonce int -- non null for internal operations
  , status smallint not null default 0 -- 0: applied, 1: backtracked, 2: failed, 3: skipped
  , error_trace jsonb
  , error_trace_ json
);
--PKEY register_global_constant_pkey; C.register_global_constant; operation_id --SEQONLY
--FKEY register_global_constant_operation_id_fkey; C.register_global_constant; operation_id; C.operation_alpha(autoid); CASCADE --SEQONLY
--FKEY register_global_constant_source_fkey; C.register_global_constant; source_id; C.addresses(address_id); CASCADE --SEQONLY
CREATE INDEX IF NOT EXISTS register_global_constant_source         on C.register_global_constant using btree (source_id); --SEQONLY --1
CREATE INDEX IF NOT EXISTS register_global_constant_gaid            on C.register_global_constant using btree (global_address_id); --SEQONLY --1


-----------------------------------------------------------------------------
-- Set_deposits_limit -- manop -- since v9.7.a

CREATE TABLE IF NOT EXISTS C.set_deposits_limit ( -- introduced by proto 12 --L
  operation_id bigint not null
  -- operation id from operation_alpha
  , source_id bigint not null
  -- source
  , value numeric
  -- value of the limit
  , consumed_milligas numeric
  -- consumed milligas
  , fee bigint not null
  -- fees
  , nonce int -- non null for internal operations
  , status smallint not null default 0 -- 0: applied, 1: backtracked, 2: failed, 3: skipped
  , error_trace jsonb
  , error_trace_ json
);
--PKEY set_deposits_limit_pkey; C.set_deposits_limit; operation_id --SEQONLY
--FKEY set_deposits_limit_operation_id_fkey; C.set_deposits_limit; operation_id; C.operation_alpha(autoid); CASCADE --SEQONLY
--FKEY set_deposits_limit_source_fkey; C.set_deposits_limit; source_id; C.addresses(address_id); CASCADE --SEQONLY
CREATE INDEX IF NOT EXISTS set_deposits_limit_source on C.set_deposits_limit using btree (source_id); --SEQONLY --1
CREATE INDEX IF NOT EXISTS set_deposits_limit_value  on C.set_deposits_limit using btree (value); --SEQONLY --1


-----------------------------------------------------------------------------
-- Tx_rollup_origination -- manop -- since v9.8.2

CREATE TABLE IF NOT EXISTS C.tx_rollup_origination ( -- introduced by proto 13 candidate --L
  operation_id bigint not null
  -- operation id from operation_alpha
  , source_id bigint not null
  -- source
  , tx_rollup_origination bigint -- since v9.9.3
  -- value of the limit
  , consumed_milligas numeric
  -- consumed milligas
  , fee bigint not null
  -- fees
  , nonce int -- non null for internal operations
  , status smallint not null default 0 -- 0: applied, 1: backtracked, 2: failed, 3: skipped
  , error_trace jsonb
  , error_trace_ json
);
--PKEY tx_rollup_origination_pkey; C.tx_rollup_origination; operation_id --SEQONLY
--FKEY tx_rollup_origination_operation_id_fkey; C.tx_rollup_origination; operation_id; C.operation_alpha(autoid); CASCADE --SEQONLY
--FKEY tx_rollup_origination_source_fkey; C.tx_rollup_origination; source_id; C.addresses(address_id); CASCADE --SEQONLY
CREATE INDEX IF NOT EXISTS tx_rollup_origination_source on C.tx_rollup_origination using btree (source_id); --SEQONLY --1
--CREATE INDEX IF NOT EXISTS tx_rollup_origination_value  on C.tx_rollup_origination using btree (tx_rollup_origination); --SEQONLY --1

-----------------------------------------------------------------------------
-- Sc_rollup_originate -- manop -- since v9.8.2

CREATE TABLE IF NOT EXISTS C.sc_rollup_originate ( -- introduced by proto 13 candidate --L
  operation_id bigint not null
  -- operation id from operation_alpha
  , source_id bigint not null
  -- source
  , address_id bigint
  -- address
  , size numeric
  -- size
  , kind text
  -- kind
  , boot_sector text
  -- boot sector
  , consumed_milligas numeric
  -- consumed milligas
  , fee bigint not null
  -- fees
  , nonce int -- non null for internal operations
  , status smallint not null default 0 -- 0: applied, 1: backtracked, 2: failed, 3: skipped
  , error_trace jsonb
  , error_trace_ json
);
--PKEY sc_rollup_originate_pkey; C.sc_rollup_originate; operation_id --SEQONLY
--FKEY sc_rollup_originate_operation_id_fkey; C.sc_rollup_originate; operation_id; C.operation_alpha(autoid); CASCADE --SEQONLY
--FKEY sc_rollup_originate_source_fkey; C.sc_rollup_originate; source_id; C.addresses(address_id); CASCADE --SEQONLY
--FKEY sc_rollup_originate_address_fkey; C.sc_rollup_originate; address_id; C.addresses(address_id); CASCADE --SEQONLY
CREATE INDEX IF NOT EXISTS sc_rollup_originate_source on C.sc_rollup_originate using btree (source_id); --SEQONLY --1


-----------------------------------------------------------------------------
-- Sc_rollup_add_messages -- manop -- since v9.9.9

CREATE TABLE IF NOT EXISTS C.sc_rollup_add_messages ( -- introduced by proto 13 candidate --L
  operation_id bigint not null
  -- operation id from operation_alpha
  , source_id bigint not null
  -- source
  , messages text[]
  , rollup_id bigint
  -- address of the rollup
  , inbox_after text
  , consumed_milligas numeric
  -- consumed milligas
  , fee bigint not null
  -- fees
  , nonce int -- non null for internal operations
  , status smallint not null default 0 -- 0: applied, 1: backtracked, 2: failed, 3: skipped
  , error_trace jsonb
  , error_trace_ json
);
--PKEY sc_rollup_add_messages_pkey; C.sc_rollup_add_messages; operation_id --SEQONLY
--FKEY sc_rollup_add_messages_operation_id_fkey; C.sc_rollup_add_messages; operation_id; C.operation_alpha(autoid); CASCADE --SEQONLY
--FKEY sc_rollup_add_messages_source_fkey; C.sc_rollup_add_messages; source_id; C.addresses(address_id); CASCADE --SEQONLY
--FKEY sc_rollup_add_messages_rollup_fkey; C.sc_rollup_add_messages; rollup_id; C.addresses(address_id); CASCADE --SEQONLY
CREATE INDEX IF NOT EXISTS sc_rollup_add_messages_source on C.sc_rollup_add_messages using btree (source_id); --SEQONLY --1


-----------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION status (s smallint)
RETURNS char
AS $$
begin
case
when s = 0 then return 'applied';
when s = 1 then return 'backtracked';
when s = 2 then return 'failed';
when s = 3 then return 'skipped';
end case;
end;
$$ LANGUAGE PLPGSQL STABLE;


-----------------------------------------------------------------------------
-- Balance: record balance diffs

-- v9.5 merged C.balance_updates_block and C.balance_updates_op into a single table, and adds column implicit_operations_results_id
CREATE TABLE IF NOT EXISTS C.balance_updates ( --L
  block_hash_id int not null,
  -- block hash
  operation_id bigint,
  -- references C.operation_alpha.autoid if any
  implicit_operations_results_id int,
  -- references C.implicit_operations_results.id if any
  balance_kind smallint not null,
  -- balance kind:
  --  0 : Contract
  --  1 : Rewards, or legacy rewards
  --  2 : Fees, or legacy fees
  --  3 : Deposits, or legacy deposits
  --  4 : Block_fees (since proto 12)
  --  5 : Nonce_revelation_rewards (since proto 12)
  --  6 : Double_signing_evidence_rewards (since proto 12)
  --  7 : Endorsing_rewards (since proto 12)
  --  8 : Baking_rewards (since proto 12)
  --  9 : Baking_bonuses (since proto 12)
  -- 10 : Storage_fees (since proto 12)
  -- 11 : Double_signing_punishments (since proto 12)
  -- 12 : Lost_endorsing_rewards (since proto 12)
  -- 13 : Liquidity_baking_subsidies (since proto 12)
  -- 14 : Burned (since proto 12)
  -- 15 : Commitments (since proto 12)
  -- 16 : Bootstrap (since proto 12)
  -- 17 : Invoice (since proto 12)
  -- 18 : Initial_commitments (since proto 12)
  -- 19 : Minted (since proto 12)
  contract_address_id bigint, -- nullable since protocol I -- since v9.9.5
  blinded_public_key_hash char(37), -- since proto 12
  -- b58check encoded address of contract(either implicit or originated)
  cycle int, -- only balance_kind 1,2,3 have cycle
  -- cycle
  diff bigint not null,
  -- balance update
  -- credited if positve
  -- debited if negative
  id int not null -- unique position within the block to allow rightful duplicates and reject wrong duplicates -- this number is ≥ 0
  , primary key (block_hash_id, id)
);
--pkey balance_updates_pkey; C.balance_updates; block_hash_id, id --SEQONLY
--FKEY balance_updates_block_hash_fkey; C.balance_updates; block_hash_id; C.block(hash_id); CASCADE --SEQONLY
--FKEY balance_updates_contract_address_fkey; C.balance_updates; contract_address_id; C.addresses(address_id); CASCADE --SEQONLY
--FKEY balance_updates_iorid_fkey; C.balance_updates; block_hash_id, implicit_operations_results_id; C.implicit_operations_results(block_hash_id, id); CASCADE --SEQONLY
CREATE INDEX IF NOT EXISTS balance_block  on C.balance_updates using btree (block_hash_id); --SEQONLY --1
CREATE INDEX IF NOT EXISTS balance_cat    on C.balance_updates using btree (balance_kind); --SEQONLY --1
CREATE INDEX IF NOT EXISTS balance_k      on C.balance_updates using btree (contract_address_id); --SEQONLY --1
CREATE INDEX IF NOT EXISTS balance_cycle  on C.balance_updates using btree (cycle); --SEQONLY --1
CREATE INDEX IF NOT EXISTS balance_operation_id on C.balance_updates using btree (operation_id); --SEQONLY --1

ALTER TABLE C.balance_updates ALTER COLUMN contract_address_id DROP NOT NULL; -- since proto 12 -- since v9.9.5
DO $$
BEGIN
ALTER TABLE C.balance_updates ADD COLUMN blinded_public_key_hash char(37);
EXCEPTION WHEN OTHERS THEN RETURN;
END $$;
--CREATE INDEX IF NOT EXISTS balance_blinded_public_key_hash on C.balance_updates using btree (blinded_public_key_hash); --SEQONLY --1


CREATE OR REPLACE FUNCTION balance_kind (bk smallint)
RETURNS char
AS
$$
SELECT coalesce ((select 'contract' where bk = 0),
       coalesce ((select 'rewards' where bk = 1),
       coalesce ((select 'fees' where bk = 2),
       coalesce ((select 'deposits' where bk = 3),
       coalesce ((select 'block_fees' where bk = 4),
       coalesce ((select 'nonce_revelation_rewards' where bk = 5),
       coalesce ((select 'double_signing_evidence_rewards' where bk = 6),
       coalesce ((select 'endorsing_rewards' where bk = 7),
       coalesce ((select 'baking_rewards' where bk = 8),
       coalesce ((select 'baking_bonuses' where bk = 9),
       coalesce ((select 'storage_fees' where bk = 10),
       coalesce ((select 'double_signing_punishments' where bk = 11),
       coalesce ((select 'lost_endorsing_rewards' where bk = 12),
       coalesce ((select 'liquidity_baking_subsidies' where bk = 13),
       coalesce ((select 'burned' where bk = 14),
       coalesce ((select 'commitments' where bk = 15),
       coalesce ((select 'bootstrap' where bk = 16),
       coalesce ((select 'invoice' where bk = 17),
       coalesce ((select 'initial_commitments' where bk = 18),
       coalesce ((select 'minted' where bk = 19)))))))))))))))))))))
$$ LANGUAGE SQL STABLE;

CREATE OR REPLACE FUNCTION balance_kind (bk int)
RETURNS char
AS $$ SELECT balance_kind (bk::smallint) $$ LANGUAGE SQL STABLE;



-----------------------------------------------------------------------------
-- Snapshot blocks
-- The snapshot block for a given cycle is obtained as follows
-- at the last block of cycle n, the snapshot block for cycle n+6 is selected
-- Use [Storage.Roll.Snapshot_for_cycle.get C.txt cycle] in proto_alpha to
-- obtain this value.
-- RPC: /chains/main/blocks/${block}/context/raw/json/cycle/${cycle}
-- where:
-- ${block} denotes a block(either by hash or level)
-- ${cycle} denotes a cycle which must be in [cycle_of(level)-5,cycle_of(level)+7]

CREATE TABLE IF NOT EXISTS C.snapshot ( --L
  cycle int,
  level int,
  primary key (cycle, level)
);

-----------------------------------------------------------------------------
-- Could be useful for baking.
-- CREATE TABLE IF NOT EXISTS delegate (
--   cycle int not null,
--   level int not null,
--   pkh char(36) not null,
--   balance bigint not null,
--   frozen_balance bigint not null,
--   staking_balance bigint not null,
--   delegated_balance bigint not null,
--   deactivated bool not null,
--   grace smallint not null,
--   primary key (cycle, pkh),
--   , foreign key (cycle, level) references snapshot(cycle, level)
--   , foreign key (pkh) references implicit(pkh)
-- );

-----------------------------------------------------------------------------
-- Delegated contract table -- NOT FILLED

CREATE TABLE IF NOT EXISTS C.delegated_contract ( --L
  delegate_id bigint,
  -- tz1 of the delegate
  delegator_id bigint,
  -- address of the delegator (for now, KT1 but this could change)
  cycle int,
  level int
  , primary key (delegate_id, delegator_id, cycle, level)
);
--PKEY delegated_contract_pkey; C.delegated_contract; delegate_id, delegator_id, cycle, level --SEQONLY
--FKEY delegated_contract_delegate_fkey; C.delegated_contract; delegate_id; C.addresses(address_id); CASCADE --SEQONLY
--FKEY delegated_contract_delegator_fkey; C.delegated_contract; delegator_id; C.addresses(address_id); CASCADE --SEQONLY
--FKEY delegated_contract_cycle_level_fkey; C.delegated_contract; cycle, level; C.snapshot(cycle, level); CASCADE --SEQONLY
CREATE INDEX IF NOT EXISTS delegated_contract_cycle     on C.delegated_contract using btree (cycle); --SEQONLY --1
CREATE INDEX IF NOT EXISTS delegated_contract_level     on C.delegated_contract using btree (level); --SEQONLY --1
CREATE INDEX IF NOT EXISTS delegated_contract_delegate  on C.delegated_contract using btree (delegate_id); --SEQONLY --1
CREATE INDEX IF NOT EXISTS delegated_contract_delegator on C.delegated_contract using btree (delegator_id); --SEQONLY --1

-----------------------------------------------------------------------------
-- Could be useful for baking.
-- CREATE TABLE IF NOT EXISTS stake (
--   delegate char(36) not null,
--   level int not null,
--   k char(36) not null,
--   kind smallint not null,
--   diff bigint not null,
--   primary key (delegate, level, k, kind, diff),
--   , foreign key (delegate) references implicit(pkh)
--   , foreign key (k) references C.addresses(address_id)
-- );
