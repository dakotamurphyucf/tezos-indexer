(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2021 Nomadic Labs, <contact@nomadic-labs.com>               *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

open Tezos_protocol_alpha.Protocol
open Tezos_baking_alpha

open Legacy_monad_globals
let ( >>= ) = Lwt.bind

(* let mainnet_protocol_heights = {
 *   proto_001 =  28082l ;
 *   proto_002 = 204761l;
 *   proto_003 = 458752l;
 *   proto_004 = 655360l;
 *   proto_005 = 851968l;
 *   proto_006 = 1212416l;
 *   proto_007 = 1343488l;
 *   proto_008 = Int32.max_int;
 *   proto_009 = Int32.max_int;
 * } *)


let hash_1 =
  Tezos_crypto.Protocol_hash.of_b58check_exn "PtCJ7pwoxe8JasnHY8YonnLYjcVHmhiARPJvqcC6VfHT5s8k8sY"
let hash_2 =
  Tezos_crypto.Protocol_hash.of_b58check_exn "PsYLVpVvgbLhAhoqAkMFUo6gudkJ9weNXhUYCiLDzcUpFpkk8Wt"
let hash_3 =
  Tezos_crypto.Protocol_hash.of_b58check_exn "PsddFKi32cMJ2qPjf43Qv5GDWLDPZb3T3bF6fLKiF5HtvHNU7aP"
let hash_4 =
  Tezos_crypto.Protocol_hash.of_b58check_exn "Pt24m4xiPbLDhVgVfABUjirbmda3yohdN82Sp9FeuAXJ4eV9otd"
let hash_5 =
  Tezos_crypto.Protocol_hash.of_b58check_exn "PsBabyM1eUXZseaJdmXFApDSBqj8YBfwELoxZHHW77EMcAbbwAS"
let hash_6 =
  Tezos_crypto.Protocol_hash.of_b58check_exn "PsCARTHAGazKbHtnKfLzQg3kms52kSRpgnDY982a9oYsSXRLQEb"
let hash_7 =
  Tezos_crypto.Protocol_hash.of_b58check_exn "PsDELPH1Kxsxt8f9eWbxQeRxkjfbxoqM52jvs5Y5fBxWWh4ifpo"
let hash_8 =
  Tezos_crypto.Protocol_hash.of_b58check_exn "PtEdo2ZkT9oKpimTah6x2embF25oss54njMuPzkJTEi5RqfdZFA"
let hash_9 =
  Tezos_crypto.Protocol_hash.of_b58check_exn "PsFLorenaUUuikDWvMDr6fGBRG8kt3e3D3fHoXK1j1BFRxeSH4i"
let hash_10 =
  Tezos_crypto.Protocol_hash.of_b58check_exn "PtGRANADsDU8R9daYKAgWnQYAJ64omN1o3KMGVCykShA97vQbvV"
let hash_11 =
  Tezos_crypto.Protocol_hash.of_b58check_exn "PtHangz2aRngywmSRGGvrcTyMbbdpWdpFKuS4uMWxg2RaH9i1qx"
let hash_12 =
  Tezos_crypto.Protocol_hash.of_b58check_exn "Psithaca2MLRFYargivpo7YvUr7wUDqyxrdhC5CQq78mRvimz6A"
let hash_alpha =
  Tezos_crypto.Protocol_hash.of_b58check_exn "ProtoALphaALphaALphaALphaALphaALphaALphaALphaDdp3zK"

let get_version conn =
  let module Conn = (val conn : Caqti_lwt.CONNECTION) in
  Conn.find_opt Db_base.Version.select ()

let record_launch dbh =
  let argv = Array.fold_left (Printf.sprintf "%s %S") "" Sys.argv in
  let module Conn = (val dbh : Caqti_lwt.CONNECTION) in
  Conn.exec Db_base.Indexer_log.record
    (Version.(Printf.sprintf "%s %s %s" t b c), argv)

let record_action action dbh =
  let module Conn = (val dbh : Caqti_lwt.CONNECTION) in
  Conn.exec Db_base.Indexer_log.record_action action

let bootstrap
    ?(exit_when_done=false)
    ?threads
    ?use_disk_cache
    ~conn ~conn2
    ~cctxt
    ~force_from ~first_alpha_level ~up_to
    ?push_start_snapshots ?push_start_watch
    ?tokens
    () =
  Verbose.CLog.printf ~vl:0 "# Starting bootstrap";
  record_action
    (Printf.sprintf "bootstrap force_from=%ld first_alpha_level=%ld up_to=%ld"
       force_from first_alpha_level up_to)
    conn2 >>= Db_base.caqti_or_fail ~__LOC__
  >>= fun () ->
  (* begin
   *   Client_baking_blocks.info cctxt (`Head 0) >>= function
   *   | Error err ->
   *     Verbose.error "# Error when trying to bootstrap: %a" pp_print_trace err;
   *     exit Verbose.ExitCodes.failure_bootstrap
   *   | (Ok { Client_baking_blocks.level ; _ }) ->
   *     Lwt.return (Alpha_context.Raw_level.to_int32 level)
   * end >>= fun current_head_level ->
   * let up_to =
   *   if up_to >= 2l then
   *     min current_head_level up_to
   *   else
   *     current_head_level
   * in
   * Verbose.CLog.printf ~vl:0 ~force:true "# Target_block=%ld" up_to; *)
  let rec fold f (accu:int32) = function
    | [] ->
      return accu
    | e::tl ->
      f accu e >>=? fun accu ->
      fold f accu tl
  in
  let proto_list : (string
                    * (?stepback:int32 ->
                       ?use_disk_cache:string ->
                       ?from:int32 ->
                       ?up_to:int32 ->
                       ?tokens:bool ->
                       first_alpha_level:int32 ->
                       ?threads:int ->
                       (#Tezos_client_001_PtCJ7pwo.Alpha_client_context.full as 'd) ->
                       (module Caqti_lwt.CONNECTION) -> _ -> int32 tzresult Lwt.t)
                   ) list =
    [
      "001_PtCJ7pwo", Tezos_indexer_001_PtCJ7pwo.Chain_db.bootstrap_chain
    ; "001_002", Chain_db_transition_001_002.bootstrap_chain
    ; "002_PsYLVpVv", Tezos_indexer_002_PsYLVpVv.Chain_db.bootstrap_chain
    ; "002_003", Chain_db_transition_002_003.bootstrap_chain
    ; "003_PsddFKi3", Tezos_indexer_003_PsddFKi3.Chain_db.bootstrap_chain
    ; "003_004", Chain_db_transition_003_004.bootstrap_chain
    ; "004_Pt24m4xi", Tezos_indexer_004_Pt24m4xi.Chain_db.bootstrap_chain
    ; "004_005", Chain_db_transition_004_005.bootstrap_chain
    ; "005_PsBabyM1", Tezos_indexer_005_PsBabyM1.Chain_db.bootstrap_chain
    ; "005_006", Chain_db_transition_005_006.bootstrap_chain
    ; "006_PsCARTHA", Tezos_indexer_006_PsCARTHA.Chain_db.bootstrap_chain
    ; "006_007", Chain_db_transition_006_007.bootstrap_chain
    ; "007_PsDELPH1", Tezos_indexer_007_PsDELPH1.Chain_db.bootstrap_chain
    ; "007_008", Chain_db_transition_007_008.bootstrap_chain
    ; "008_PtEdo2Zk", Tezos_indexer_008_PtEdo2Zk.Chain_db.bootstrap_chain
    ; "008_009", Chain_db_transition_008_009.bootstrap_chain
    ; "009_PsFLoren", Tezos_indexer_009_PsFLoren.Chain_db.bootstrap_chain
    ; "009_010", Chain_db_transition_009_010.bootstrap_chain
    ; "010_PtGRANAD", Tezos_indexer_010_PtGRANAD.Chain_db.bootstrap_chain
    ; "010_011", Chain_db_transition_010_011.bootstrap_chain
    ; "011_PtHangz2", Tezos_indexer_011_PtHangz2.Chain_db.bootstrap_chain
    ; "011_012", Chain_db_transition_011_012.bootstrap_chain
    ; "012_Psithaca", Tezos_indexer_012_Psithaca.Chain_db.bootstrap_chain
(**# ifdef __NOALPHA__ (\*# <-- this is converted to "open comment section" #**)
     ; "012_A", Chain_db_transition_012_A.bootstrap_chain
     ; "alpha", Tezos_indexer_alpha.Chain_db.bootstrap_chain
(**# ifdef __NOALPHA__ *) #**)
    ]
  in
  fold
    (fun last_treated_block
      (proto,
       (f : (?stepback:int32 ->
             ?use_disk_cache:string ->
             ?from:int32 ->
             ?up_to:int32 ->
             ?tokens:bool ->
             first_alpha_level:int32 ->
             ?threads:int ->
             Tezos_client_007_PsDELPH1.Protocol_client_context.wrap_full ->
             (module Caqti_lwt.CONNECTION) ->
             (module Caqti_lwt.CONNECTION) ->
             int32 tzresult Lwt.t)))
      ->
        Verbose.Debug.printf ~vl:0 "%s last_treated_block=%ld proto=%s" __LOC__
          last_treated_block proto;
        let from, stepback =
          if force_from < 0l && last_treated_block < up_to then
            Some last_treated_block, Some force_from
          else if force_from <= 0l then
            None, None
          else
            Some (max last_treated_block force_from), None
        in
        let first_alpha_level = max first_alpha_level last_treated_block in
        record_action
          (Format.asprintf
             "bootstrapping %s stepback=%a from=%a up_to=%ld first_alpha_level=%ld"
             proto
             (fun out e -> match e with None -> Format.fprintf out "None" | Some e -> Format.fprintf out "%ld" e) stepback
             (fun out e -> match e with None -> Format.fprintf out "None" | Some e -> Format.fprintf out "%ld" e) from
             up_to
             first_alpha_level
          )
          conn >>= Db_base.caqti_or_fail ~__LOC__
        >>= fun () ->
        f
          ?stepback
          ?from
          ?use_disk_cache
          ~up_to
          ~first_alpha_level
          ?tokens
          ?threads
          cctxt
          conn
          conn2
    )
    first_alpha_level
    proto_list
  >>=? fun last_treated_block ->
  Verbose.Debug.printf ~vl:0 "[bootstrap] last_treated_block=%ld" last_treated_block;
  if exit_when_done then
    begin
      Verbose.CLog.printf ~force:true "Done bootstrapping, exiting now as you asked for.";
      exit 0;
    end;
  Option.iter (fun e -> Lwt.wakeup e last_treated_block) push_start_watch ;
  Option.iter (fun e -> Lwt.wakeup e ())  push_start_snapshots;
  return_unit

let snapshots pool no_snapshot_blocks snapshot_blocks_only start_snapshots cctxt =
  if no_snapshot_blocks then
    return ()
  else
    (Caqti_lwt.Pool.use (record_action "snapshots") pool) >>= Db_base.caqti_or_fail ~__LOC__ >>= fun () ->
    let rec get_snapshots cycle =
      (
        Snapshot_utils_6.get_roll_snapshot_for_cycle cycle cctxt
        >>= function
        | Error _ ->
          Snapshot_utils_7.get_roll_snapshot_for_cycle cycle cctxt
        | Ok x -> return x
      )
      >>= function
      | Ok block ->
        let block = Int32.of_int block in
        Verbose.Debug.printf ~vl:0 "Snapshot: cycle=%d block=%ld" cycle block;
        Tezos_indexer_lib.Db_base.Snapshot_table.store_snapshot_levels
          pool [cycle, block]
        >>= fun () ->
        Verbose.Debug.printf ~vl:0 "Getting next snapshot with cycle=%d" (succ cycle);
        get_snapshots (succ cycle)
      | Error _ ->
        Lwt_unix.sleep 60. >>= fun () ->
        get_snapshots cycle
    in
    (if snapshot_blocks_only then
       Lwt.return_unit
     else
       start_snapshots) >>= fun () ->
    get_snapshots 7

let watch
    ~heads_only
    ~no_watch
    ~watch_hook
    cctxt
    conn conn2 pool
    use_disk_cache first_alpha_level start_watch
    tokens ~do_not_get_missing_scripts
    ~up_to =

  let fold_f block result =
    match block, result with
    | _, Error err
    | Error err, _ ->
      Verbose.Debug.eprintf ~vl:1 "Error at %s" __LOC__;
      Verbose.error "%a" pp_print_trace err ;
      exit 1
    | Ok ({ Client_baking_blocks.hash ;
            chain_id = _; predecessor = _; fitness = _; timestamp = _;
            protocol ; next_protocol ; proto_level = _; level ;
            context = _; _ ;
          }), Ok () ->
      match
        Tezos_indexer_lib.Misc.list_assoc_opt (protocol, next_protocol)
          [
            (* most probable *)
            (hash_11, hash_11),
            (fun () -> Tezos_indexer_011_PtHangz2.Chain_db.store_block_full
                ~block:(`Hash (hash, 0)) ~tokens cctxt conn);
            (hash_12, hash_12),
            (fun () -> Tezos_indexer_012_Psithaca.Chain_db.store_block_full
                ~block:(`Hash (hash, 0)) ~tokens cctxt conn);
(**# ifdef __NOALPHA__ (\*# <-- this is converted to "open comment section" #**)
            (hash_alpha, hash_alpha),
            (fun () -> Tezos_indexer_alpha.Chain_db.store_block_full
                ~block:(`Hash (hash, 0)) ~tokens cctxt conn);
(**# ifdef __NOALPHA__ *) #**)
            (hash_10, hash_10),
            (fun () -> Tezos_indexer_010_PtGRANAD.Chain_db.store_block_full
                ~block:(`Hash (hash, 0)) ~tokens cctxt conn);
            (hash_11, hash_12),
            (fun () -> Chain_db_transition_011_012.store_block_full
                ~block:(`Hash (hash, 0)) ~tokens cctxt conn);
            (hash_10, hash_11),
            (fun () -> Chain_db_transition_010_011.store_block_full
                ~block:(`Hash (hash, 0)) ~tokens cctxt conn);
            (hash_12, hash_alpha),
            (fun () -> Chain_db_transition_012_A.store_block_full
                ~block:(`Hash (hash, 0)) ~tokens cctxt conn);
            (hash_9, hash_9),
            (fun () -> Tezos_indexer_009_PsFLoren.Chain_db.store_block_full
                ~block:(`Hash (hash, 0)) ~tokens cctxt conn);
            (hash_8, hash_8),
            (fun () -> Tezos_indexer_008_PtEdo2Zk.Chain_db.store_block_full
                ~block:(`Hash (hash, 0)) ~tokens cctxt conn);
            (hash_8, hash_9),
            (fun () -> Chain_db_transition_008_009.store_block_full
                ~block:(`Hash (hash, 0)) ~tokens cctxt conn);
            (hash_7, hash_7),
            (fun () -> Tezos_indexer_007_PsDELPH1.Chain_db.store_block_full
                ~block:(`Hash (hash, 0)) ~tokens cctxt conn);
            (hash_6, hash_6),
            (fun () -> Tezos_indexer_006_PsCARTHA.Chain_db.store_block_full
                ~block:(`Hash (hash, 0)) ~tokens cctxt conn) ;
            (* least probable *)
            (hash_5, hash_5),
            (fun () -> Tezos_indexer_005_PsBabyM1.Chain_db.store_block_full
                ~block:(`Hash (hash, 0)) ~tokens cctxt conn) ;
            (hash_4, hash_4),
            (fun () -> Tezos_indexer_004_Pt24m4xi.Chain_db.store_block_full
                ~block:(`Hash (hash, 0)) ~tokens cctxt conn) ;
            (hash_3, hash_3),
            (fun () -> Tezos_indexer_003_PsddFKi3.Chain_db.store_block_full
                ~block:(`Hash (hash, 0)) ~tokens cctxt conn) ;
            (hash_2, hash_2),
            (fun () -> Tezos_indexer_002_PsYLVpVv.Chain_db.store_block_full
                ~block:(`Hash (hash, 0)) ~tokens cctxt conn) ;
            (hash_1, hash_1),
            (fun () -> Tezos_indexer_001_PtCJ7pwo.Chain_db.store_block_full
                ~block:(`Hash (hash, 0)) ~tokens cctxt conn) ;
            (* even less probable, since these transitions happen at most once per chain *)
            (hash_6, hash_7),
            (fun () -> Chain_db_transition_006_007.store_block_full
                ~block:(`Hash (hash, 0)) ~tokens cctxt conn);
            (hash_7, hash_8),
            (fun () -> Chain_db_transition_007_008.store_block_full
                ~block:(`Hash (hash, 0)) ~tokens cctxt conn);
            (hash_5, hash_6),
            (fun () -> Chain_db_transition_005_006.store_block_full
                ~block:(`Hash (hash, 0)) ~tokens cctxt conn) ;
            (hash_4, hash_5),
            (fun () -> Chain_db_transition_004_005.store_block_full
                ~block:(`Hash (hash, 0)) ~tokens cctxt conn) ;
            (hash_3, hash_4),
            (fun () -> Chain_db_transition_003_004.store_block_full
                ~block:(`Hash (hash, 0)) ~tokens cctxt conn) ;
            (hash_2, hash_3),
            (fun () -> Chain_db_transition_002_003.store_block_full
                ~block:(`Hash (hash, 0)) ~tokens cctxt conn) ;
            (hash_1, hash_2),
            (fun () -> Chain_db_transition_001_002.store_block_full
                ~block:(`Hash (hash, 0)) ~tokens cctxt conn) ;
          ]
      with
      | None ->
        Verbose.error "Unsupported protocol";
        exit 1
      | Some do_store_block ->
        begin
          Lwt.catch do_store_block
            (function
              | Tezos_indexer_lib.Misc.Reorg _ -> return (Int32.neg @@ Alpha_context.Raw_level.to_int32 level)
              | e -> Lwt.fail e)
        end >>= function
        | Error e ->
          Verbose.Debug.eprintf ~vl:1 "Error at %s" __LOC__;
          Verbose.error "Error when recording block %a: %a"
            Block_hash.pp hash
            pp_print_trace e;
          exit 1
        | Ok bhid ->
          begin
            let level = Alpha_context.Raw_level.to_int32 level in
            if bhid <> level then
              let module Conn = (val conn : Caqti_lwt.CONNECTION) in
              Verbose.Debug.printf ~vl:0
                "Block (block_hash=%a level=%ld) failed to be recorded (bhid=%ld). Reorganization may have happened."
                Block_hash.pp hash
                level
                bhid;
              Conn.rollback () >>= fun _ ->
              Conn.find_opt Db_base.Block_table.delete_from_level level >>=
              Caqti_lwt.or_fail >>= function
              | None ->
                Verbose.error "DB returned an error after trying to delete blocks due to a chain reorganization.";
                exit 1
              | Some force_from ->
                if force_from <> Int32.pred level then
                  Verbose.Debug.eprintf ~vl:0
                    "After chain reorganization, \
                     force_from (%ld) <> Int32.pred level (%ld)"
                    (force_from) (Int32.pred level);
                record_action
                  (Printf.sprintf "reorg happened force_from=%ld level=%ld" force_from level)
                  conn >>= Db_base.caqti_or_fail ~__LOC__
                >>= fun () ->
                let start = Unix.gettimeofday () in
                bootstrap
                  (* reorganization happened *)
                  ~exit_when_done:false
                  (* ~force_from:(min force_from (Int32.pred level)) <-- wrong *)
                  ~force_from:0l (* the bootstrap function will figure out by itself where to resume *)
                  ~conn ~conn2(* :conn *) ~cctxt ~first_alpha_level
                  ~tokens
                  ~up_to:Int32.max_int
                  ()
                >>=? fun () ->
                record_action
                  (Printf.sprintf "bootstrap (after reorg at %ld) done in %f seconds (going back to watching)" level (Unix.gettimeofday () -. start))
                  conn
                >>= Db_base.caqti_or_fail ~__LOC__ >>= fun () ->
                return_unit
            else
              return_unit
          end >>=? fun () ->
          watch_hook () >>= function
          | None -> return_unit
          | Some i ->
            Verbose.CLog.eprintf "Your hook was executed and its return code is %d" i;
            return_unit
  in (* end of fold_f *)
  Verbose.Debug.printf ~vl:0 "Start watching";
  begin
    (* ************************************ *)
    (* HERE we wait until bootstrap is over *)
    (* ************************************ *)
    start_watch
    >>= fun last_treated_block ->
    (* When we arrive here, bootstrap is done. *)
    if no_watch then
      exit 0;
    if last_treated_block >= up_to then
      exit 0;
    begin
      if heads_only then
        Client_baking_blocks.monitor_heads
          cctxt
          ~next_protocols:None
          `Main
      else
        Client_baking_blocks.monitor_valid_blocks
          cctxt ~next_protocols:None ()
    end >>=? fun stream ->
    Verbose.Debug.printf ~vl:0 "Start monitoring blocks";
    Lwt.catch begin fun () ->
      Lwt_stream.peek stream >>= function
      | None ->
        Verbose.error "Failed to peak block stream from node. I'll exit now.";
        (* this should never happen unless the connection to the node or the node
           stops working *)
        Stdlib.exit 1
      | Some (Error e)  ->
        Verbose.Debug.eprintf ~vl:1 "Error at %s" __LOC__;
        Verbose.error "Error when tried to peak stream: %a" pp_print_trace e;
        exit 1
      | Some Ok { Client_baking_blocks.level ; _ } ->
        let level = Alpha_context.Raw_level.to_int32 level in
        (if level > last_treated_block then
           (* If ever bootstrap didn't go high enough, we try again: *)
           bootstrap
             (* second bootstrap *)
             ~exit_when_done:false
             ~force_from:(Int32.pred last_treated_block)
             ?use_disk_cache
             ~conn ~conn2(* :conn *)
             ~cctxt ~first_alpha_level
             ~tokens
             ~up_to:(Int32.max_int) (* <- if we needed to stop earlier, we would have exited already *)
             ()
         else
           return_unit) >>= function
        | Ok () ->
          if not do_not_get_missing_scripts then
            Lwt.async (fun () -> Babylon_scripts.update_scripts cctxt pool);
          (* TODO: add context *) record_action "watch" conn >>= Db_base.caqti_or_fail ~__LOC__ >>= fun () ->
          Lwt_stream.fold_s fold_f stream (Ok ())
        | Error e ->
          Verbose.Debug.eprintf ~vl:1 "Error at %s" __LOC__;
          Verbose.error "Error while bootstrapping: %a" pp_print_trace e;
          exit 1
    end begin function
      | exn ->
        Verbose.error
          "An unhandled exception happened, I'll exit now: %s"
          (Printexc.to_string exn);
        exit 1
    end
  end


let do_mempool_indexing pool cctxt =
  let prev = ref `_0 in
  let new_proto = ref true in
  let level = ref 0l in
  let one b _r = match b with
    | (Error e)  ->
      Verbose.Debug.eprintf ~vl:1 "Error at %s" __LOC__;
      Verbose.Debug.error "Error when tried to peak stream: %a" pp_print_trace e;
      Lwt.return (Error e)
    | (Ok { Client_baking_blocks.protocol ; next_protocol ; level = bl ; _ }) ->
      level := Alpha_context.Raw_level.to_int32 bl;
      begin
        (* FIXME: make the order more relevant. Should start with highest protocol and go backwards. *)
        if (protocol, next_protocol) = (hash_10, hash_10) then
          if !prev = `_10 then
            return ()
          else
            begin
              prev := `_10;
              new_proto := true;
              Lwt.async (fun () ->
                  Verbose.CLog.printf ~vl:0 "Watching mempool proto 10";
                  Tezos_indexer_010_PtGRANAD.Mempool_utils.watch_mempool ~pool cctxt new_proto level
                  >>= fun _ -> Lwt.return_unit);
              return ()
            end
        else if (protocol, next_protocol) = (hash_11, hash_11)
             || (protocol, next_protocol) = (hash_10, hash_11) then
          if !prev = `_11 then
            return ()
          else
            begin
              prev := `_11;
              new_proto := true;
              Lwt.async (fun () ->
                  Verbose.CLog.printf ~vl:0 "Watching mempool proto 11";
                  Tezos_indexer_011_PtHangz2.Mempool_utils.watch_mempool ~pool cctxt new_proto level
                  >>= fun _ -> Lwt.return_unit);
              return ()
            end
        else if (protocol, next_protocol) = (hash_12, hash_12)
             || (protocol, next_protocol) = (hash_11, hash_12) then
          if !prev = `_12 then
            return ()
          else
            begin
              prev := `_12;
              new_proto := true;
              Lwt.async (fun () ->
                  Verbose.CLog.printf ~vl:0 "Watching mempool proto 12";
                  Tezos_indexer_012_Psithaca.Mempool_utils.watch_mempool ~pool cctxt new_proto level
                  >>= fun _ -> Lwt.return_unit);
              return ()
            end
(**# ifdef __NOALPHA__ (\*# <-- this is converted to "open comment section" #**)
        else if (protocol, next_protocol) = (hash_alpha, hash_alpha)
             || (protocol, next_protocol) = (hash_12, hash_alpha) then
          if !prev = `_alpha then
            return ()
          else
            begin
              prev := `_alpha;
              new_proto := true;
              Lwt.async (fun () ->
                  Verbose.CLog.printf ~vl:0 "Watching mempool proto 12";
                  Tezos_indexer_alpha.Mempool_utils.watch_mempool ~pool cctxt new_proto level
                  >>= fun _ -> Lwt.return_unit);
              return ()
            end
(**# ifdef __NOALPHA__ *) #**)
        else
          begin
            Verbose.error "Unsupported protocols for monitoring mempool: %a %a. Please make sure your node is synchronized!"
              Protocol_hash.pp protocol
              Protocol_hash.pp next_protocol;
            Stdlib.exit 1
          end
      end
  in
  Client_baking_blocks.info cctxt (`Head 0) >>= fun b ->
  one b () >>= fun _ ->
  Client_baking_blocks.monitor_valid_blocks
    cctxt ~next_protocols:None ()
  >>=? fun stream ->
  Lwt_stream.fold_s one stream (Ok ())



let update_balances no_balance_updates ?(limit=10000) pool cctxt =
  let exception Break in
  let run pool =
    Caqti_lwt.Pool.use (fun conn ->
        let f get_balanceless_contracts record_contract_balance () =
          let rec iter protocol_number =
            if protocol_number < 0 then
              Lwt.return_unit
            else
              begin
                let module Conn = (val conn : Caqti_lwt.CONNECTION) in
                Verbose.Debug.printf ~vl:3 "Getting balanceless contracts with protocol #%d, limit=%d" protocol_number limit;
                Conn.collect_list
                  get_balanceless_contracts
                  (protocol_number, limit)
                >>= Db_base.caqti_or_fail ~__LOC__ >>= function
                | [] ->
                  (* wait a few seconds if there's no contract balance to be filled *)
                  Lwt_unix.sleep 3. >>= fun () ->
                  iter (pred protocol_number)
                | to_be_filled ->
                  Conn.find_opt Db_base.Block_table.select_max_level () >>=
                  Db_base.caqti_or_fail ~__LOC__ >>= function
                  | None ->
                    Verbose.Debug.error
                      "Block table is empty but you have entries in contract balances. \
                       This should not be possible unless you have a corrupted DB.";
                    Stdlib.exit 1
                  | Some (db_max_level) ->
                    let counter = ref 0 in
                    Lwt.catch (fun () ->
                        Tezos_indexer_lib.Misc.limited_list_iter_p
                          (fun ((k, kid), (bh, bhid, level)) ->
                             Caqti_lwt.Pool.use (fun conn ->
                                 begin
                                   incr counter;
                                   ((record_contract_balance conn cctxt ~bhid ~bh ~level ~k ~kid ~db_max_level) : _ result Environment.Lwt.t)
                                   >>= function
                                   | Ok _ -> Lwt.return_unit
                                   | Error _ ->
                                     Lwt.fail Break
                                 end
                                 >>= fun _ -> return ()
                               )
                               pool
                             >>= fun _ -> Lwt.return_unit
                          ) to_be_filled
                        >>= fun () -> Lwt.return true
                      )
                      (function _e ->
                         (* Verbose.error "%s" (Printexc.to_string _e); *)
                         Lwt.return false)
                    >>= fun continue ->
                    if continue && !counter > 0 && !counter <= limit then
                      iter protocol_number (* repeat, keep same protocol *)
                    else
                      iter (pred protocol_number)
              end
          in
          iter 11
        in
        let rec loop () =
          f Tezos_indexer_010_PtGRANAD.Db_alpha.Contract_balance_table.get_balanceless_contracts
            Tezos_indexer_010_PtGRANAD.Chain_db.record_contract_balance () >>=
          (* the first '()' is necessary, do not repeat it, since its absence replaces 'fun () ->' *)
          f Tezos_indexer_012_Psithaca.Db_alpha.Contract_balance_table.get_balanceless_contracts
            Tezos_indexer_012_Psithaca.Chain_db.record_contract_balance >>=
          f Tezos_indexer_011_PtHangz2.Db_alpha.Contract_balance_table.get_balanceless_contracts
            Tezos_indexer_011_PtHangz2.Chain_db.record_contract_balance >>=
(**# ifdef __NOALPHA__ (\*# <-- this is converted to "open comment section" #**)
          f Tezos_indexer_alpha.Db_alpha.Contract_balance_table.get_balanceless_contracts
            Tezos_indexer_alpha.Chain_db.record_contract_balance >>=
(**# ifdef __NOALPHA__ *) #**)
          f Tezos_indexer_009_PsFLoren.Db_alpha.Contract_balance_table.get_balanceless_contracts
            Tezos_indexer_009_PsFLoren.Chain_db.record_contract_balance >>=
          f Tezos_indexer_008_PtEdo2Zk.Db_alpha.Contract_balance_table.get_balanceless_contracts
            Tezos_indexer_008_PtEdo2Zk.Chain_db.record_contract_balance >>=
          f Tezos_indexer_007_PsDELPH1.Db_alpha.Contract_balance_table.get_balanceless_contracts
            Tezos_indexer_007_PsDELPH1.Chain_db.record_contract_balance >>=
          f Tezos_indexer_006_PsCARTHA.Db_alpha.Contract_balance_table.get_balanceless_contracts
            Tezos_indexer_006_PsCARTHA.Chain_db.record_contract_balance >>=
          f Tezos_indexer_005_PsBabyM1.Db_alpha.Contract_balance_table.get_balanceless_contracts
            Tezos_indexer_005_PsBabyM1.Chain_db.record_contract_balance >>=
          f Tezos_indexer_004_Pt24m4xi.Db_alpha.Contract_balance_table.get_balanceless_contracts
            Tezos_indexer_004_Pt24m4xi.Chain_db.record_contract_balance >>=
          f Tezos_indexer_003_PsddFKi3.Db_alpha.Contract_balance_table.get_balanceless_contracts
            Tezos_indexer_003_PsddFKi3.Chain_db.record_contract_balance >>=
          f Tezos_indexer_002_PsYLVpVv.Db_alpha.Contract_balance_table.get_balanceless_contracts
            Tezos_indexer_002_PsYLVpVv.Chain_db.record_contract_balance >>=
          f Tezos_indexer_001_PtCJ7pwo.Db_alpha.Contract_balance_table.get_balanceless_contracts
            Tezos_indexer_001_PtCJ7pwo.Chain_db.record_contract_balance >>=
          loop
        in
        loop ()
      ) pool
  in
  if no_balance_updates then
    return_unit
  else
    run pool >>=
    Db_base.caqti_or_fail ~__LOC__ >>=
    return




let store_block_header conn cctxt n =
  Client_baking_blocks.info cctxt (`Level n) >>= function
  | Ok {
      hash (* : Block_hash.t *);
      chain_id = _ (* : Chain_id.t *);
      predecessor (* : Block_hash.t *);
      fitness (* : Bytes.t list *);
      timestamp (* : Time.Protocol.t *);
      protocol = _ (* : Protocol_hash.t *);
      next_protocol = _ (* : Protocol_hash.t *);
      proto_level (* : int *);
      level (* : Raw_level.t *);
      context (* : Context_hash.t *);
      (* predecessor_block_metadata_hash = _ ; *)
      (* predecessor_operations_metadata_hash = _ ; *)
    } ->
    let module Conn = (val conn : Caqti_lwt.CONNECTION) in
    Conn.find
      Db_base.Block_table.insert0
      (hash, { level = Alpha_context.Raw_level.to_int32 level
             ; proto_level ; predecessor ; timestamp ; fitness
             ; validation_passes = 0
             ; operations_hash=Operation_list_list_hash.empty;context })
    >>= Db_base.caqti_or_fail ~__LOC__ >>=
    begin function
      | Some x when x = n ->
        Verbose.Debug.block_stored hash n;
        Lwt.return_unit
      | _ ->
        Verbose.error "Error: failed to insert block %ld's header" n;
        exit 1
    end
  | Error (((RPC_client_errors.Request_failed {uri=_; error=Connection_failed msg; meth=_})::_) as em) ->
    Verbose.Debug.eprintf ~vl:1 "Error at %s" __LOC__;
    Verbose.error "Unable to connect to the node: \"%s\"" msg;
    Verbose.eprintf ~vl:1 "Details: %a" pp_print_trace em;
    exit 1
  | Error e ->
    Verbose.Debug.eprintf ~vl:1 "Error at %s" __LOC__;
    Verbose.error "[Error] %a" pp_print_trace e;
    exit 1
