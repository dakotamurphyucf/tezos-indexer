#!/bin/bash
# Open Source License
# Copyright (c) 2021 Nomadic Labs <contact@nomadic-labs.com>
# Copyright (c) 2021 Philippe Wang <philippe.wang@gmail.com>
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.

if [[ "$INDEXER_MULTICORE2" == "true" ]]
then
 exec bash utils/run-tezos-indexer-multicore2.bash "$@"
fi

if [[ ${MULTISEGMENT_TEZOS_INDEXER+true} != "true" ]]
then
    exec $(dirname $(dirname "$0"))/tezos-indexer "$@"
fi

set -xe

epoch_start=$(date +%s)

###################################################################################
########## database
HOST=${POSTGRES_HOST:-localhost}
DB=${DB_NAME:$USER}
PORT=${POSTGRES_PORT:-5432}
PG_USER=${POSTGRES_USER:-tezos}

DB_USER=${DB_USER:-phil}
export PGPASSWORD=${DB_PASS:-phil}
PSQL_OPTIONS="-a"

########## node
TEZOS_NODE=${TEZOS_NODE:-"http://localhost:8732"}
TEZOS_NODE1=${TEZOS_NODE1:-$TEZOS_NODE}
TEZOS_NODE2=${TEZOS_NODE2:-$TEZOS_NODE1}
TEZOS_NODE3=${TEZOS_NODE3:-$TEZOS_NODE2}
TEZOS_NODE4=${TEZOS_NODE4:-$TEZOS_NODE3}
TEZOS_NODE5=${TEZOS_NODE5:-$TEZOS_NODE4}
TEZOS_NODE6=${TEZOS_NODE6:-$TEZOS_NODE5}
TEZOS_NODE7=${TEZOS_NODE7:-$TEZOS_NODE6}
TEZOS_NODE8=${TEZOS_NODE8:-$TEZOS_NODE7}
TEZOS_NODE9=${TEZOS_NODE9:-$TEZOS_NODE8}
TEZOS_NODEA=${TEZOS_NODEA:-$TEZOS_NODE9}

echo "${TEZOS_NODES[@]}"

TEZOS_NODES_COUNT=${#TEZOS_NODES[@]}
if (( TEZOS_NODES_COUNT == 0 ))
then
    TEZOS_NODES=($TEZOS_NODE1 $TEZOS_NODE2 $TEZOS_NODE3 $TEZOS_NODE4 $TEZOS_NODE5 $TEZOS_NODE6 $TEZOS_NODE7 $TEZOS_NODE8 $TEZOS_NODE9 $TEZOS_NODEA)
elif (( TEZOS_NODES_COUNT == 1 ))
then
    IFS_=$IFS
    IFS="$IFS ,;"
    X=$TEZOS_NODES
    TEZOS_NODES=()
    for i in $X ; do TEZOS_NODES+=($i) ; done
    IFS=$IFS_
fi

echo "${TEZOS_NODES[@]}"

###################################################################################

# Override settings 1/2:
override="$(dirname "$0")/run-tezos-indexer-multicore.override.bash"
if [[ -f "$override" ]]
then
    . "$override"
fi

########## indexer
TI=${INDEXER_SRC_PATH:-$(dirname $(dirname "$0"))}
LOGS_DIR=${LOGS_DIR:-"/var/log/tezos-indexer"}

BLOCK_CACHE_DIR=${BLOCK_CACHE_DIR:-"/blocks"} # no space in the path or it might fail

INDEXER_DATAKINDS=${INDEXER_DATAKINDS:-"--no-contract-balances --no-snapshot --do-not-get-missing-scripts --no-watch --tokens-support"}
INDEXER_RUN=${INDEXER_RUN:-"--use-disk-cache --debug --verbosity 1 --verbose --timeout 300s --auto-recovery-credit 1000 --make-conn2"}
INDEXER_OTHER_OPTIONS=${INDEXER_OTHER_OPTIONS:-""}

########## parallelism
gsize=${TEZOS_INDEXER_SEGMENT_SIZE:-2000} # size of a segment of the chain

top=${TEZOS_TOP:-$top} # last block to index
if [[ "$top" == "auto" ]] || [[ "$top" == "" ]] ; then
    top_level=$(curl -s ${TEZOS_NODES[0]}/chains/main/blocks/head | jq .header.level)
    top=$((top_level / gsize * gsize))
fi
if [[ $((top*1)) == 0 ]] ; then
    >&2 echo "Error: TEZOS_TOP has a wrong value, or TEZOS_NODE is unreachable, or jq is not installed. I'll exit now."
    exit 1
fi


# It seems the best speed is achieved with a number of parallel jobs equal to 1.5 times the number of logical cores:

if [[ "${PROCESSES}" == "" ]] ; then
    if [[ -f /proc/cpuinfo ]] ; then
        PROCESSES=$(grep processor /proc/cpuinfo | wc -l)
    else
        PROCESSES=$(sysctl -n hw.ncpu)
    fi
    ((PROCESSES= PROCESSES * 15 / 10))
    if (( PROCESSES < 1 )) ; then
        PROCESSES=4 ;
    fi
fi
if (( PROCESSES == 0 )) ; then
    PROCESSES=1
fi

RANDFILE=${RANDFILE:-/dev/zero}

###################################################################################

# Override settings 2/2:
override="$(dirname "$0")/run-tezos-indexer-multicore.override.bash"
if [[ -f "$override" ]]
then
    . "$override"
fi

###################################################################################
# If you need to modify something below this line, please consider opening a merge request!
###################################################################################
mkdir -p $LOGS_DIR
mkdir -p $BLOCK_CACHE_DIR

PSQL="time psql -h $HOST -p $PORT -U $DB_USER $DB $PSQL_OPTIONS"
LOGS="$LOGS_DIR"/indexer-segment-$gsize-$HOST-$DB

INDEXER_OPTIONS_ARRAY_SIZE=${#INDEXER_OPTIONS_ARRAY[@]}
if (( INDEXER_OPTIONS_ARRAY_SIZE == 0 ))
then
    INDEXER_OPTIONS_BASE="$INDEXER_DATAKINDS $INDEXER_RUN $INDEXER_OTHER_OPTIONS --db=postgresql://$DB_USER@$HOST:$PORT/$DB --block-cache-directory=$BLOCK_CACHE_DIR"
    for TEZOS_NODE in ${TEZOS_NODES[@]} ; do
        INDEXER_OPTIONS_ARRAY+=("$INDEXER_OPTIONS_BASE --tezos-url=$TEZOS_NODE")
    done
fi


dropdb   -h $HOST -p $PORT -U $PG_USER $DB || true
createdb -h $HOST -p $PORT -U $PG_USER -O $DB_USER $DB

# make sure we're in "known territory" (this is meant to avoid ending up in some random directory)
cd $TI

epoch_start_multicore_schema=$(date +%s)
make db-schema-all-multicore | $PSQL
epoch_end_multicore_schema=$(date +%s)


if [[ "$ADDRESS_CACHE" == "true" ]]
then
    # get the list of all existing contract addresses
    epoch_start_list_contracts=$(date +%s)
    if ! [[ -f "$BLOCK_CACHE_DIR/list_of_contracts-$top.txt" ]]
    then
        (curl -s ${TEZOS_NODE[0]}/chains/main/blocks/$top/context/contracts | tr -d '"[]'  | tr ',' '\n' ; cat $BLOCK_CACHE_DIR/addresses) |sort -u | awk "{print \$0\" \"NR}" >  "$BLOCK_CACHE_DIR/list_of_contracts-$top.txt"
    fi
    epoch_start_record_contracts_list=$(date +%s)
    # record all existing contract addresses
    echo "\\copy c.addresses (address, address_id) from '$BLOCK_CACHE_DIR/list_of_contracts-$top.txt' DELIMITER ' ';" | $PSQL
fi

(( iterations = top / gsize ))

set +x
(
echo -n "all: do-boot "
# Starting with the most recent blocks goes faster. But random segments gives better load balancing.
middle=$(( (iterations+1) / 2 ))
echo -n $(for (( i=iterations-1; i>=middle; i-- )) ; do echo "do-$i "; done)
echo -n " " $(for (( i=middle-1; i>=0; i-- )) ; do echo "do-$i "; done)
# echo -n $(for (( i=iterations-1; i>=middle; i-- )) ; do echo "do-$i "; done | sort -R --random-source=$RANDFILE)
# echo -n " " $(for (( i=middle-1; i>=0; i-- )) ; do echo "do-$i "; done | sort -R --random-source=$RANDFILE)
echo

echo
echo "do-boot:"
printf "\t%s\n" "PGPASSWORD=$PGPASSWORD time $TI/tezos-indexer --force-from=0 --bootstrap-upto=1 ${INDEXER_OPTIONS_ARRAY[0]} > $LOGS-0-1.log 2> $LOGS-0-1.error.log"
echo -e '\t@echo Target \"do-boot\" done: $$(date)'

echo
INDEXER_OPTIONS_ARRAY_SIZE=${#INDEXER_OPTIONS_ARRAY[@]}
for (( i=0; i<iterations; i++ )) ; do
    size=$gsize
    start=$(( i * size + 1 ))
    if (( start == 1 ))
    then
        start=2
    fi
    (( end = (i+1) * size + 1 ))
    echo "do-$i:"
    INDEXER_OPTIONS=${INDEXER_OPTIONS_ARRAY[$((i%INDEXER_OPTIONS_ARRAY_SIZE))]}
    printf "\t%s\n" "PGPASSWORD=$PGPASSWORD time $TI/tezos-indexer --force-from=$start --bootstrap-upto=$end $INDEXER_OPTIONS > $LOGS-$start-$end.log 2> $LOGS-$start-$end.error.log || (x=\$\$? ; tail $LOGS-$start-$end.error.log && exit \$\$x)"
    echo -e '\t@echo Target \"do-'$i'\" done: $$(date)'
done ) > temporary-Makefile
start="$(date)"

epoch_start_indexing=$(date +%s)

if (( PROCESSES <= 1 )) ;
then
    PGPASSWORD=$PGPASSWORD time $TI/tezos-indexer $INDEXER_OPTIONS1 > $LOGS-solo-multicore.log 2> $LOGS-solo-multicore.error.log
else
    make all -j "$PROCESSES" -f temporary-Makefile
fi

epoch_start_db_convert=$(date +%s)

end="$(date)"
echo "Indexing started on $start - ended on $end"
if [[ "${NO_DB_CONVERSION}" != "true" ]]
then
    time make db-schema-all-default | $PSQL
fi
echo "Indexing started on $start - ended on $end"

epoch_end=$(date +%s)

function pretty_time () {
    seconds=$1
    hours=$(( seconds / 3600 ))
    minutes=$(( (seconds % 3600) / 60 ))
    remaining_seconds=$(( seconds % 60 ))
    if (( hours > 0 )) ; then echo -n ${hours}h ; fi
    if (( hours > 0 )) || (( minutes > 0 )) ; then echo -n ${minutes}m ; fi
    echo ${remaining_seconds}s
}

total_operations="$($PSQL <<< 'select count(*) from c.operation_alpha;' | grep -vE '[(c-]')"

set +e
tee -a "$LOGS+benchmarks.txt" <<EOF
#####################################
Database:
HOST=$HOST
DB=$DB
PORT=$PORT
PG_USER=$PG_USER
DB_USER=$DB_USER
PSQL_OPTIONS=$PSQL_OPTION
PSQL=$PSQL

Tezos nodes:
TEZOS_NODES=($(for i in "${TEZOS_NODES[@]}" ; do echo -n "\"$i\" " ; done))

Tezos-indexer:
TI=$TI
LOGS_DIR=$LOGS_DIR
BLOCK_CACHE_DIR=$BLOCK_CACHE_DIR
INDEXER_DATAKINDS=$INDEXER_DATAKINDS
INDEXER_RUN=$INDEXER_RUN
INDEXER_OTHER_OPTIONS=$INDEXER_OTHER_OPTIONS
gsize=$gsize
top=$top
PROCESSES=$PROCESSES
LOGS_DIR=$LOGS_DIR
BLOCK_CACHE_DIR=$BLOCK_CACHE_DIR
LOGS=$LOGS
INDEXER_OPTIONS_ARRAY=($(for i in "${INDEXER_OPTIONS_ARRAY[@]}" ; do echo -n "\"$i\" " ; done))

Durations:
STARTED ON $(date -d @$epoch_start) -- TOTAL DURATION: $(pretty_time $((epoch_end-epoch_start)))
- apply DB schema from $(date -d @$epoch_start_multicore_schema) -- duration: $(pretty_time $((epoch_end_multicore_schema-epoch_start_multicore_schema)))
EOF
[[ "$ADDRESS_CACHE" == true ]] && tee -a "$LOGS+benchmarks.txt" <<EOF
- list contract addresses from $(date -d @$epoch_start_list_contracts) -- duration: $(pretty_time $((epoch_start_record_contracts_list-epoch_start_list_contracts)))
- record contract addresses into DB from $(date -d @$epoch_start_list_contracts) -- duration: $(pretty_time $((epoch_start_indexing-epoch_start_record_contracts_list)))
EOF
tee -a "$LOGS+benchmarks.txt" <<EOF
- start indexing from $(date -d @$epoch_start_indexing) -- duration: $(pretty_time $((epoch_start_db_convert-epoch_start_indexing)))
- conversion of DB from $(date -d @$epoch_start_db_convert) -- duration: $(pretty_time $((epoch_end-epoch_start_db_convert)))
- average speed: $((top/(epoch_end-epoch_start))) blocks / second, or $(((60*top)/(epoch_end-epoch_start))) blocks per minute, $((total_operations / (epoch_end-epoch_start))) operations per second

$(w|grep -o load.*)
############################
EOF

if [[ $# -gt 0 ]] ; then
    $TI/tezos-indexer "$@"
else
    true
fi
