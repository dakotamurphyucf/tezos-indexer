-- Open Source License
-- Copyright (c) 2021 Nomadic Labs <contact@nomadic-labs.com>
--
-- Permission is hereby granted, free of charge, to any person obtaining a
-- copy of this software and associated documentation files (the "Software"),
-- to deal in the Software without restriction, including without limitation
-- the rights to use, copy, modify, merge, publish, distribute, sublicense,
-- and/or sell copies of the Software, and to permit persons to whom the
-- Software is furnished to do so, subject to the following conditions:
--
-- The above copyright notice and this permission notice shall be included
-- in all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
-- THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
-- FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
-- DEALINGS IN THE SOFTWARE.


CREATE OR REPLACE FUNCTION div_no_error (a numeric, b numeric)
RETURNS numeric
AS $$
BEGIN
if b = 0 then return null;
else return a / b;
end if;
END;
$$ LANGUAGE PLPGSQL;

DROP VIEW IF EXISTS time_between_blocks;
CREATE OR REPLACE VIEW time_between_blocks AS
SELECT
 extract(epoch from ("timestamp" - (coalesce ((select "timestamp" from c.block b where b.level = x.level - 1),
                          "timestamp"))))
 as time_since_previous_block
 , (select count(*) from c.operation_alpha where block_hash_id = x.level)
   as number_of_operations
 , div_no_error ((select count(*) from c.operation_alpha where block_hash_id = x.level)::numeric, (extract(epoch from ("timestamp" - (coalesce ((select "timestamp" from c.block b where b.level = x.level - 1),
                          "timestamp")))))::numeric)
   as operations_per_second
 , *
FROM C.block x order by level desc;
select * from time_between_blocks limit 10000;

-- operation production on average for the 100 fastest blocks amongst the latest 100000 blocks
with y as (with x as (select * from time_between_blocks limit 100000) select * from x order by operations_per_second desc limit 100) select sum(operations_per_second) / 100 from y;
-- operation production on average for the latest 100000 blocks
with x as (select * from time_between_blocks limit 100000) select sum(operations_per_second) / 100000 from x;
-- fastest block amongst the latest 100000 blocks
with x as (select * from time_between_blocks limit 100000) select operations_per_second from x order by operations_per_second desc limit 1;
