# Open Source License
# Copyright (c) 2019-2021 Nomadic Labs <contact@nomadic-labs.com>
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.

FROM registry.gitlab.com/tezos/opam-repository:runtime-build-dependencies--e7846067dc70b1fb27133ecc16e96ab21c2d8501 as build
# cf. tezos/.gitlab/ci/templates.yml or tezos/scripts/version.sh

USER root
RUN apk add opam alpine-sdk m4 gmp-dev postgresql-dev perl libev-dev libffi libffi-dev postgresql-client coreutils
RUN apk add --no-cache -X http://dl-cdn.alpinelinux.org/alpine/edge/community hidapi-dev

USER tezos
WORKDIR /home/tezos
ENV OPAMYES=true

# To update the opam-list file (after updating this file), run `make update-opam-list`

RUN opam remote add default https://opam.ocaml.org

COPY *.Makefile opam-list ./

RUN make tezos -f tezos.Makefile

RUN eval $(opam env) && make install-opam-deps -f opam.Makefile

COPY .git .git
USER root
RUN chown tezos /home/tezos/.git
USER tezos

RUN git checkout src utils/run-tezos-indexer-multicore.bash Makefile && eval $(opam env) && make protos && make build && ln -f ./_build/default/src/bin_indexer/main_indexer.exe tezos-indexer && /home/tezos/tezos-indexer --version
ENTRYPOINT ["/home/tezos/utils/run-tezos-indexer-multicore.bash"]
